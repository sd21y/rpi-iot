# **Off-Grid IoT device Project**
----

1. [デバイスの目的](./iot-purpose.md)
2. [電源系](./iot-power.md)
3. [本体・計測系](./iot-device.md)
4. [間欠動作用プログラム](./iot-interval.md)
5. [計測データの可視化](./iot-munin.md)
6. [筐体](./iot-case.md)
7. [Raspberry pi の使い方色々](./iot-raspberrypi.md)
8. [IoTにおけるArduino その１](./iot-arduino1.md)
9. [IoTにおけるArduino その２](./iot-arduino2.md)
10. [IoTにおけるArduino その３](./iot-arduino3.md)
11. [I2Cで使えるセンサー](./i2c-sensor.md)
12. [Raspberry piでArduino IDEを使う](./arduino-IDE on raspberry pi.md)
13. [Raspberry pi + MovidiusでAI](./raspberrypi-movidius.md)
14. [おまけ](./iot-etc.md)
15. [参考URL](./iot-reference.md)
----

