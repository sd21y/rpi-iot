#!/bin/bash

maddr="a-shigi@sgmail.jp"
vlimit=12.0

# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

#測定
dt=`date "+%Y/%m/%dT%H:%M:%S"`
ptm=`./bme280-api.py` #気圧・気温・湿度測定
volt=`./ina226.py`    #電源電圧測定
wget -q -O /dev/null "https://sgmail.jp/demo/iot-ptmv.cgi?d=$dt&$ptm&v=$volt"
echo "d=$dt&$ptm&v=$volt" > ./ptmv.txt
scp ./ptmv.txt pi3.local:. > /dev/null 2>&1
#
#exit
#
offtime=`hexsec 30`
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x11 0x00 0x10 $offtime 0x00 i > /dev/null 2>&1 #30秒後にoff
  res=$?
  sleep 0.5
  if [ $res = 0 ]; then
    break
  fi
done
#電池電圧チェック
if [ `echo "$volt > $vlimit" | bc` == 1 ]; then
  #電池電圧が$vlimit以上ある時
  pontime=`hexsec 3440`
  while :
  do
    /usr/sbin/i2cset -y 1 0x28 0x10 0x00 0x10 $pontime 0x00 i > /dev/null 2>&1 #3600-150秒後にon
    res=$?
    sleep 0.5
    if [ $res = 0 ]; then
      break
    fi
  done
  #
  while :
  do
    /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x02 i > /dev/null 2>&1 #LED点滅
    res=$?
    sleep 0.5
    if [ $res = 0 ]; then
      break
    fi
  done
else
  #電池電圧が$vlimit以下の時
  #電池交換メールを送信
  #mail $maddr -s "$HOSTNAME is low battery" < ./mail.txt
  while :
  do
    /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x00 i > /dev/null 2>&1 #LED消灯
    res=$?
    sleep 0.5
    if [ $res = 0 ]; then
      break
    fi
  done
fi
#シャットダウン
/sbin/poweroff
