#!/usr/bin/ruby

interval = 60
limit = 0
time = 0
File.open("/mnt/usbsd/share/capacity.txt", "r") {|fd| limit = (fd.gets().to_f * 21.6).to_i }
begin
  File.open("/mnt/usbsd/share/uptime.txt","r") {|fd| time  = fd.gets().to_i }
rescue
  time = 0
end
time += interval
File.open("/mnt/usbsd/share/uptime.txt","w")  {|fd| fd.puts(time.to_s) }
if(time >= limit)
  File.unlink("/mnt/usbsd/share/uptime.txt")
  `/sbin/poweroff`
end
if(File.exist?("/mnt/usbsd/share/poweroff.txt"))
  `/sbin/poweroff`
end
`./battery.sh |  nkf -Lw -W8 -s > /mnt/usbsd/share/battery.txt`
