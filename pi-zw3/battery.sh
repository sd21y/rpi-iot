#!/bin/bash

# 全容量計算
cap=`cat /mnt/usbsd/share/capacity.txt`
ul=`echo "${cap} * 21.6" | bc`
cmin=`echo "${ul} / 60" | bc`
ch=`echo "${cmin} / 60" | bc`
cm=`echo "${cmin} % 60" | bc`

# 使用時間計算
if [ -e /mnt/usbsd/share/uptime.txt ]; then
  ut=`cat /mnt/usbsd/share/uptime.txt`
  umin=`echo "${ut} / 60" | bc`
  uh=`echo "${umin} / 60" | bc`
  um=`echo "${umin} % 60" | bc`
  ur=`echo "scale=2; ${umin} / ${cmin} * 100" | bc`
  lr=`echo "scale=2; 100 - ${ur}" | bc`
else
  uh=0
  um=0
  ur=0
  lr=100
fi

# 残り時間計算
if [ -e /mnt/usbsd/share/uptime.txt ]; then
  ut=`cat /mnt/usbsd/share/uptime.txt`
  lmin=`echo "(${ul} - ${ut}) / 60" | bc`
  lh=`echo "${lmin} / 60" | bc`
  lm=`echo "${lmin} % 60" | bc`
else
  lh=0
  lm=0
fi
#
printf "容量:        %02d時間%02d分 (%dmAh)\n" ${ch} ${cm} ${cap}
printf "使用: %5.1f%% %02d時間%02d分\n" ${ur} ${uh} ${um}
printf "残り: %5.1f%% %02d時間%02d分\n" ${lr} ${lh} ${lm}
