#!/bin/bash

/usr/sbin/sendmail -t <<EOS
From: root
To: root
Subject: $HOSTNAME Start battery drive
Content-Type: text/plain; charset="UTF-8"

バッテリー駆動を開始します。

`./battery.sh`
EOS
/usr/sbin/service postfix restart
sleep 10
/usr/sbin/postqueue -f
