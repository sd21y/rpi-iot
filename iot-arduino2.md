[TOC]

# ■ IoTにおけるArduino その２

------

## ▼ Arduinoで使用したセンサー

Arduinoで使用できるセンサーはたくさんあるが今回使用したセンサーは以下のセンサーである。

- **BME280(I2C)** 気温、湿度、気圧
- **INA226(I2C)** 電圧、電流、電力
- **LIS3DH (I2C)** 加速度
- **ADT7410 (I2C)** 温度
- **DS18x20 (1-wire)** 温度

## ▼ NTPサーバについて

計測を行う場合、計測時刻は非常に重要である。そのため、本システムではNTPサーバにアクセスし、時刻の同期を行っている。この時NTPサーバとしてntp.nict.jpなどの公開サーバを使うのが一般的だとは思うのだが、時刻同期の頻度が高い場合には、自前のNTPサーバを立てて使用したほうが良い。というのも、NTPサーバにもよるが、同一IPアドレスからのアクセス回数に制限を設けていることがあるからである。

## ▼ ESP32の注意点

NodeMCU-32Sなどの評価用devkitを使う場合、どのピンが使えてどのピンが使えないかをきちんと把握しておかなければならない。

**NodeMCU-32S**

![odemcu-32s_pin](./img/nodemcu-32s_pinout.jpg)

NodeMCU-32Sのピンは上の画像のような配置になっているのだが、このうち**GPIO06～GPIO11**は内部で使用しているのでなにも**接続してはいけない**。なのにボード上にはピンが出ているので、うっかりIO端子として使用してしまうことがある。なので、このピンはニッパで切断して使えなくしてしまうのが良いと思う。

また、

**Wemos Lolin32 OLED**

![olin32oled](./img/lolin32oled-pin.jpg)

はNodeMCU-32Sとはピンの配置が違うので同じESP32を使用していると言っても同一のGPIOが使えるとは限らない。同じスケッチを使えるようにするには、共通して利用可能なピンを使うなどの工夫が必要になる。

Lolin32ではOLEDのインターフェースにI2Cを使用しており、内部的に使用しているので、I2C接続は**GPIO5,GPIO4**固定になる。そのため、他のI2Cを使うセンサーも同じピンを使用しなければならない。

ところが、多くのArduinoのライブラリでは、**GPIO21,GPIO22**を標準のI2Cピンとして使用していることが多く、内部的に固定になっているライブラリを使う場合は、ライブラリのコードを改変するか、別の同等のピン固定ではないライブラリを使用するなどの方策が必要である。

## ▼ SSD1331カラーOLEDディスプレイ

![mg_004](./img/img_0043.jpg)

**Wemos Lolin32 OLED**にはモノクロの**SSD1306**というコントローラが使われていたが、最近では**SSD1331**とカラーOLEDを組み合わせたモジュールも[市販](https://www.amazon.co.jp/gp/product/B01M8JCPYX/ref=oh_aui_detailpage_o04_s00?ie=UTF8&amp;amp;psc=1)されている。このモジュールはSPI接続のものが多いので、SPIで接続してみたのが上の写真である。まだESP32で使用するためのライブラリが揃っていないようで、

```C++
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1331.h>
#include <SPI.h>
```

Adafruitのライブラリを使おうとすると、コンパイルエラーになる。どうやら**ESP8266**までは対応しているようなのだが、ESP32にはまだ対応していないようだ。とりあえず、動作確認のため、他のライブラリを探して動作確認を行った。見つけたライブラリはグラフィック表示はできるものの、テキストの表示ができない(フォントのビットマップデータを用意すれば可能ではあるが)のでもう少しなんとかならないか試行錯誤中である。

## ▼ ESP32の電源について

先に書いた[NodeMCU-32Sを使った測定回路](./iot-arduino.md)では電源回路がなかなか難しい。というのも**ESP32**のみならず、**PIX-MT100**の電源も管理する必要があるのだが、

- PIX-MT100の消費電力が大きい(ピークで5V,1500mA程度)
- NodeMCU自体も消費電力が大きい(ピークで3.3V,1000mA程度)
- 降圧で駆動するか、昇圧で駆動するかの選択

などの問題があり、実際に回路を作って実行すると誤動作が頻発するのである。動作状況を観察すると、

- 5Vの電源電圧がピーク時に3V付近まで落ち込みPIX-MT100が誤動作する
- バッテリー電圧が低下した場合、電源供給能力が不足する

といった現象が発生しているようだ。

まあ、電源に使用している電源モジュールが怪しいチャイナモジュールということもあるのだが、やはりPIX-MT100などを駆動するにはギリギリの電源供給能力であることが判明したわけである。

ではどうするか？電源モジュールの容量を上げれば良いのだが、価格との兼ね合いで安価なチャイナモジュールも捨てがたいものがあり、苦肉の策として電気二重層コンデンサをバッテリーにかましてピーク時の電力を賄う方法を使用している。

ただし、現在はリチウムイオン電池２直列で使用しているため、電気二重層コンデンサは２つ直列に接続しないと耐圧が不足するのだが、太陽電池等で駆動することも考えると４直列以上にしないと耐圧が不足する可能性がある。電気二重層コンデンサは耐圧を少しでも超えると簡単に壊れてしまうので、扱いが難しい。

逆に電源電圧を5.5V以下に抑えることができれば小さな電気二重層コンデンサでもOKなので、昇圧型の電源モジュールを使用して5V,3.3Vの電源を供給することも検討している。

## ▼ ということで

![システム構成図](./img/soler-esp32.jpg)

このような構成を考えてみた。このような構成にすることで、太陽電池でLiPoバッテリーに充電しつつNodeMCU-32Sを駆動することができる。また、チャージコントローラとLiPoバッテリー充電コントローラ部分を切り離せばスタンドアロンでも動作する。

## ▼ ~~3.3V駆動の罠~~

で上記のような構成にした場合、5V駆動では発生しない問題にぶち当たった。どんな問題かと言うと、**昇圧型DC-DCコンバータ**を**Nch MOSFET**でON/OFFすると、**DC-DCコンバータが意図した電圧を出力しない**という問題である。

これはスイッチングに購入していた**Nch MOSFET**が**4V駆動**であるために起こっていたのではないかと思われる。~~つまり、本来は**Vgs**に**4V**以上の電圧を掛けないとフルスイッチしないFETに3.3V程度の電圧しかかかっていなかったことが判明したのである。~~

~~なんとかならんものかと探していたら、[NchパワーMOSFET(100V17A) IRLU3410PBF](http://akizukidenshi.com/catalog/g/gI-06025/)というFETを見つけた。このFETだと**Vgsが2V以上**あればフルスイッチするようである。[赤外線リモコン出力にFETを使ってみた](http://hello-world.blog.so-net.ne.jp/2013-01-06)というブログにもあるように乾電池２本程度の低電圧駆動に好適なFETのようである。~~

~~なので、早速注文して試してみるつもりである。まあ、これがうまくいかないと、上のシステムは瓦解してしまうので、なんとかしたいと思っている。~~

※上記の考察は間違い。回路接続の間違いできちんとした電圧がかかっていなかったのが原因と判明した。

![img_0049](./img/img_0049.jpg)

ということで、上記のブロック図に従って作成したのが上の写真の回路である。写真では切れているが、右側にPIX-MT100がつながっている。初期の実行テストは成功し、現在ランニングテスト中である。電源の安定化のため、電池のラインに**0.1F**の電気二重層コンデンサを接続している。

## ▼ 5V供給に適したDC-DCコンバータ

上記の構成でPIX-MT100を駆動するために使用しているDC-DCコンバータは本来、モバイルバッテリーの5V電源供給用として作られたものなので、良いかなと思っていたのだが、実際に動かしてみるとギリギリの状態で動作しているようだ。そのため、太陽電池からの5Vを供給している間は、きちんと動作するが、リチウムイオン電池だけにすると、PIX-MT100がきちんと起動しないと言う状況に陥ることがわかった。

使用しているDC-DCコンバータを見ると、FP6298というシルク印刷があるので、ググってみたところ、[FP6298の検索]([https://www.google.co.jp/search?q=FP6298&ie=utf-8&oe=utf-8&hl=ja)という状況で、どの程度の電源供給能力があるのかが良くわからない。ということで、以前購入していた[TPS6300](http://www.tij.co.jp/product/jp/TPS63000)というチップを使用した[リチウムポリマー／乾電池向け昇降圧DC-DCモジュール](https://strawberry-linux.com/catalog/items?code=12055)を使ってみることにした。

![img_0062](./img/img_0062.jpg)

本番回路と同様にNch-MOSFETを使ってESP32のIOポートでON/OFFしてみたところ(上の写真)、電池駆動のみで正常にPIX-MT100を起動することができた。数回ON/OFFを行ってみたが、いずれも正常に起動するので、これを使った回路にしたほうが良いと思われる。

## ▼ 犯人は18650チャージボード

上記の回路がうまく動き、やったねと思っていたのだが、もしかすると犯人は**18650チャージボード**かもしれないということに気がついた。本番回路同様、18650チャージボードにバッテリーを接続し、テストしてみると、**出力電流が足りない！**という状態になってしまった。どうやらチャージボードの**過電流保護回路**が働いてしまっているようだ。ということで、また0.1Fの電気二重層コンデンサを復活させてみると正常に動くようである。うーみゅ、このチャージボードが使えないのは非常に痛い。なにせ、**100円**程度で入手できるので、非常にコストパフォーマンスが良いのだ。TPS6300を使った昇圧回路のほうが効率が良く、消費電流を低減できるので、なんとか実用になるかもしれない。もう少し模索してみることにする。

## ▼ チャージボードの回路

![5885.970x0](./img/5885.970x0.jpg)

チャージボードのICのシルク印刷から回路図を検索して見つけたのがこれ。TP4056が充電コントローラ、DW01Aが過放電防止用の保護回路のようだ。**OUT-**端子に流れる電流を**DW01A**が**FS8205A**を介してコントロールする形になっている。FS8205Aには２つFETが入っていて片方が逆流保護、もう一つが過電流保護の働きをしているものと思われる。この回路を眺めていると、**B-**と**OUT-**をつなぐと保護用のFS8205Aがバイパスされてしまい、過電流保護が働かなくなるはず。本来の使い方ではないが、PIX-MT100のような大電流を消費する場合には保護回路のバイパスもありかと思われる。

![img_0064](./img/img_0064.jpg)

上はバッテリーチャージャーのクローズアップ写真であるが、使用している部品もその配置もほとんど同じである。が右と左のボードでは、よく見ると回路図の**R5とR6が逆**についている。どちらが回路図と合っているかと言えば、左のボードである。実際、動作も違っていて、右のボードはUSBに電源を繋がないと、OUT側に出力がないが、左のボードはバッテリーを繋ぐだけで最初からOUT側に出力が出るようになっている。うーむ、チャイナボードは動けばOKというアバウトなものが多いなあ。

## ▼ バイパスは有効か？

ということで、早速保護回路をバイパスしてみたのだが、結果的には変化なし、もしくは悪化という結論に終わった。USBタイプの簡易電圧電流計をつないで確認すると、PIX-MT100を負荷としてつないだ場合、起動途中に１秒程度であるが3V以下に電圧が下がってしまう(=消費電力が大きい)タイミングがあり、これを乗り越えることができれば正常に起動できるという状況になっているようだ。

この電圧降下を避けるには、USB給電用のDC-DCコンバータの電源変動に対する応答を早くする、電力変動吸収用のキャパシタをつける、などの方策があると思われるが、とりあえず手っ取り早くできる方法として**MOSFETスイッチの後段**に0.1Fの電気二重層コンデンサを移動した。目論見としてはバッテリー端にキャパシタを追加するより、より負荷に近い部分につけたほうが有効であろうということなのだが、欠点としては、キャパシタがスイッチの後に付いているので、電源を入れるのに時間がかかるようになること、電源を切ってもすぐに電圧が下がらないことである。とりあえず、この回路でバッテリーのみを使用してNodeMCU-32SとPIX-MT100を駆動したところ、正常に起動することを確認した。

![img_0063](./img/img_0063.jpg)

上の写真がこの回路である。この回路で再度いろいろとテストしていくつもりである。

## ▼ やっぱり駄目なこともある

ということで、時間をおいて数回起動テストをやってみたが、5V直前に入れた0.1Fでは足りないことがあるようである。仕方がないので、バッテリー端子部分に1Fのキャパシタを追加してみた。現在のところ動作は比較的良好である。

## ▼ ESP32スケッチ

ということで、動作を確認したスケッチを以下に示す。

**TimerSampler.ino**

```C++
/*
  timer wakeup measerement base
*/

#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>

// #define LOCAL

//
// define macros
//

#define uS_TO_S_FACTOR      1000000UL           // Conversion factor for micro seconds to seconds
#define JST                 (9 * 3600L)         // JST offset
#define TIME_TO_ON          (60)                // MT100 startup wait time
#define WAKE_OFFSET         (15)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (60 * 60)           // Time ESP32 will go to sleep (in seconds)

//
// ssid and password list
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

#if defined(LOCAL)
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "********", "********" },
  { NULL, NULL }
};
#else
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "********", "********" },
  { NULL, NULL }
};
#endif

//
// ntp server list
//

#if defined(LOCAL)
const char *ntp_hosts[] = {                     // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#else
const char *ntp_hosts[] = {                     // NTP servers
  "sgmail.jp",
  "sgmail.jp",
  "sgmail.jp",
  "time.google.com",
  "ntp.nict.jp",
  NULL
};
#endif

//
// global objects
//

const char *http_server     = "https://sgmail.jp/cgi/esp32.cgi";       // data logging server name

WiFiMulti wifiMulti;                            // Multi AP
esp_sleep_wakeup_cause_t wakeup_reason;         // Wakeup reason
RTC_DATA_ATTR int bootCount = 0;                // sleep times counter
int lte_power               = 23;               // GPIO23 lte_power on digital port
int lte_power_adc           = 36;               // GPIO36 lte power read analog port

struct tm timeInfo, timeMeas, timeFine;         // times
float battery_volts         = 0.0;              // Battery voltage
int epoch_left;                                 // epoch seconds
int wifi_connect_count      = 0;                //

// BME280
int bme280_avalable;                            // BME280 device avalable
float bme280_temp;
float bme280_press;
float bme280_humidity;
float core_temp;

//
// show wakeup reason
//

void get_wakeup_reason() {
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch (wakeup_reason) {
    case 1  : Serial.print("Wakeup caused by external signal using RTC_IO\n"); break;
    case 2  : Serial.print("Wakeup caused by external signal using RTC_CNTL\n"); break;
    case 3  : Serial.print("Wakeup caused by timer\n"); break;
    case 4  : Serial.print("Wakeup caused by touchpad\n"); break;
    case 5  : Serial.print("Wakeup caused by ULP program\n"); break;
    default : {
        Serial.print("Wakeup was not caused by deep sleep\n");
        break;
      }
  }
}

//
// setup hardware
//

void hw_init() {
  Serial.begin(115200);
  get_wakeup_reason();
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(lte_power, OUTPUT);
  pinMode(lte_power_adc, INPUT);
  delay(1 * 1000);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(lte_power, HIGH);
  bme280_avalable = bme280_init();
  bootCount ++;
  Serial.printf("Boot count : %d\n", bootCount);
}

//
// read battery volts and halt then low battery
//

void read_power_volts() {
  int adc_value = analogRead(lte_power_adc);
  Serial.printf("adc : %04d\n", adc_value);
  battery_volts = (adc_value * 2) * (3.3 / 4096);
  Serial.printf("power adc value : %04d,voltage : %5.2fV\n", adc_value, battery_volts);
}

//
// wait LTE dongle is up
//

void wait_lte_up() {
  Serial.printf("wait for MT100 LTE up %d sec.\n", TIME_TO_ON);
  delay((TIME_TO_ON - 1) * 1000); //Take some time to open up the Serial Monitor
}

//
// setup ap list
//

void wifimulti_init() {
  int i;
  int status;
  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  wait_lte_up();
  Serial.print("WiFi connect start\n");
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  i = 0;
  if ((status = wifiMulti.run()) != WL_CONNECTED) {
    Serial.printf("status : %d\n", status);
    if (i > 20) {
      Serial.print("Bad ssid esp restart\n");
      digitalWrite(lte_power, LOW);
      delay(1000);
      esp_sleep_enable_timer_wakeup(1000);
      esp_deep_sleep_start();
      Serial.print("This will never be printed\n");
      // ESP.restart();
    }
    i ++;
    delay(1000);
  }
}

//
// get ntp date or rtc date
//

void ntp_init() {
  if (true) {
    // if (wakeup_reason != 3 || (bootCount % 12) == 0) {
    configTime(JST, 0, ntp_hosts[0], ntp_hosts[1], ntp_hosts[2]);
    getLocalTime(&timeInfo);
    Serial.printf("NTP date : %04d-%02d-%02d %02d:%02d:%02d\n",
                  timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    if ((timeInfo.tm_year + 1900) < 1971) {
      Serial.print("Can not NTP sync, force restart\n");
      digitalWrite(lte_power, LOW);
      delay(1000);
      ESP.restart();
    }
  } else {
    configTime(JST, 0, NULL, NULL, NULL);
  }
}

//
// idle for measure epoch time
//

int wait_for_measure() {
  time_t now, left;
  now = time(NULL);
  left = TIME_TO_SLEEP - (now % TIME_TO_SLEEP);
  Serial.printf("Wait for epoch : %d - (%ld %% %d) = %d sec.\n", TIME_TO_SLEEP, now, TIME_TO_SLEEP, left);
  if (left > ((TIME_TO_ON + WAKE_OFFSET) * 2)) {
    left -= TIME_TO_ON + WAKE_OFFSET;
    Serial.printf("Deep sleep for %ld sec.\n", left);
    esp_sleep_enable_timer_wakeup(left * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    Serial.print("This will never be printed\n");
  } else {
    Serial.printf("Wait by delay : %d sec.\n", left);
    epoch_left = left;
    delay(left * 1000);
    for (;;) {
      getLocalTime(&timeInfo);
      if (timeInfo.tm_sec == 0) break;
      delay(10);
    }
  }
  getLocalTime(&timeInfo);
  Serial.printf("Epoch time : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
}

//
// measure data
//

void measure(void) {
  getLocalTime(&timeMeas);
  Serial.printf("Measure start : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeMeas.tm_year + 1900, timeMeas.tm_mon + 1, timeMeas.tm_mday, timeMeas.tm_hour, timeMeas.tm_min, timeMeas.tm_sec);
  //
  if (bme280_avalable == 0) bme280_getValues();
  core_temp = temperatureRead();
  //
  getLocalTime(&timeFine);
  Serial.printf("Measure end   : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeFine.tm_year + 1900, timeFine.tm_mon + 1, timeFine.tm_mday, timeFine.tm_hour, timeFine.tm_min, timeFine.tm_sec);
}

//
// send measure data via http
//

void http_send() {
  HTTPClient http;
  int httpCode;
  char buffer[256];
  Serial.print("http send start\n");
  sprintf(buffer,
          "%s?"
          "date=%04d-%02d-%02dT%02d:%02d:%02d&"
          "boot=%d&"
          "battery=%f&"
          "latency=%d&"
          "core_temp=%f&"
          "temp=%f&"
          "pressure=%f&"
          "humidity=%f",
          http_server,
          timeMeas.tm_year + 1900, timeMeas.tm_mon + 1, timeMeas.tm_mday, timeMeas.tm_hour, timeMeas.tm_min, timeMeas.tm_sec,
          bootCount,
          battery_volts,
          epoch_left,
          core_temp,
          bme280_temp,
          bme280_press,
          bme280_humidity);
  Serial.printf("%s\n", buffer);
  for (int i = 0; i < 5; i ++) {
    http.begin(buffer);
    http.setTimeout(10000);
    httpCode = http.GET();
    if (httpCode > 0) {
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        Serial.println("Payload : " + payload);
        http.end();
        return;
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      if (httpCode == HTTPC_ERROR_READ_TIMEOUT) {
        http.end();
        return;
      }
    }
    http.end();
    Serial.print("http send retry after 10 sec.\n");
    delay(10000);
  }
}

//
// deep sleep for next time
//

void deep_sleep_next() {
  time_t now, left;
  now = time(NULL);
  left = (TIME_TO_SLEEP - (now % TIME_TO_SLEEP)) - (TIME_TO_ON + WAKE_OFFSET);
  if (left < 0) {
    delay(-left * 1000);
    now = time(NULL);
    left = (TIME_TO_SLEEP - (now % TIME_TO_SLEEP)) - (TIME_TO_ON + WAKE_OFFSET);
  }
  Serial.printf("Deep sleep for %ld sec.\n", left);
  Serial.print("Going to sleep now\n");
  esp_sleep_enable_timer_wakeup((unsigned long)left * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
  Serial.print("This will never be printed\n");
}

//
// setup main
//

void setup() {
  hw_init();
  read_power_volts();
  wifimulti_init();
  ntp_init();
  wait_for_measure();
  measure();
}

//
// main loop, send data via http
//

void loop() {
  http_send();
  deep_sleep_next();
}
```

**bme280.ino**

```C++
#include <Wire.h>
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"

#define I2C_SDA 25
#define I2C_SCL 21
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)
#define BME280_ADD 0x76

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

void bme280_getValues() {
  bme280_temp = bme.readTemperature();
  bme280_press = bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT;
  bme280_humidity = bme.readHumidity();
  Serial.printf("気温 = % 5.1f ºC\n", bme280_temp);
  Serial.printf("気圧 = % 5.1f hPa\n",bme280_press);
  Serial.printf("湿度 = % 5.1f %%\n", bme280_humidity);
}

int bme280_init() {
  bool status;
  status = bme.begin(BME280_ADD);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    return -1;
  }
  return 0;
}
```

また、サーバ側でHTTPリクエストを受け付けるコードはRubyで作成した。

**esp32.cgi**

```ruby
#!/usr/bin/ruby

require 'cgi'

cgi = CGI.new()
temp = File.dirname(__FILE__) + "/" + File.basename(__FILE__,".cgi") + ".tmp"
data = File.dirname(__FILE__) + "/" + File.basename(__FILE__,".cgi") + ".dat"
File.open(temp,"w") {|fd|
  cgi.params.each {|k,v|
    fd.puts "#{k}:#{v.join(",")}"
  }
}
File.rename(temp,data)
cgi.out({"type"=>"text/plain","connection"=>"close"}) { "OK" }
`./esp32-mail.sh`
```

**esp32-mail.sh**

```bash
#!/bin/bash
LANG=C
DATE=`date +'%a, %d %b %Y %H:%M %z'`
/usr/sbin/sendmail -t <<EOS
From: a.shigi@gmail.com
To: shigi8086@gmail.com
Date: ${DATE}
Subject: ${HOSTNAME} 計測データ更新
Content-Type: text/plain; charset="UTF-8"

計測データを更新しました。

---
`cat esp32.dat`
---

https://sgmail.jp/munin/measure-day.html
EOS
```

muninとのインターフェースは、次のようなmunin-pluginを用意し、

**/etc/munin/plugins/bme280-pressure**

```bash
#!/bin/bash

if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi
if [ "$1" = "config" ]; then
  echo 'graph_title BME280 気圧(-1000hPa)'
  echo 'graph_args --base 1000 -r'
  echo 'graph_vlabel 気圧(-1000hPa)'
  echo 'graph_scale no'
  echo 'graph_category Measure'
  echo 'graph_info BME280 気圧(-1000hPa)'
  echo 'temp.label BME280 気圧(-1000hPa)'
  #echo 'temp.warning 13'
  echo 'temp.info bme280.py'
  # Last, if run with the "config"-parameter, quit here (don't
  # display any data)
  exit 0
fi
echo -n "temp.value "
echo "`/var/www/cgi/esp32.rb pressure` - 1000.0" | bc
```

次のようなrubyスクリプトで実際のデータを渡すようにしている。

**esp32.rb**

```ruby
#!/usr/bin/ruby

data = File.dirname(__FILE__) + "/" + File.basename(__FILE__,".rb") + ".dat"
v = "NaN"
begin
  File.open(data,"r") {|fd|
    while(line = fd.gets()) do
      ARGV.each {|k|
        if(line =~ /^#{k}:(.*?)$/)
          v = $1
        end
      }
    end
  }
rescue
end
puts v
```

## ▼ 原価はいくら？

上記の構成で、果たしていくら掛かるのか？原価を計算してみる。

| 部品                                  | 数量 |       価格 |
| :------------------------------------ | :--: | ---------: |
| **基本構成**                          |      |            |
| NodeMCU-32S                           |  1   |      1,400 |
| 18650電池ケース                       |  1   |        100 |
| 18650リチウムイオン電池               |  2   |      2,400 |
| 低損失三端子レギュレーター TA48M033F  |  1   |        150 |
| BME280センサー                        |  1   |      1,100 |
| 電気二重層コンデンサ 0.1F             |  1   |        200 |
| プルアップ用抵抗(10KOhm)              |  2   |         50 |
| **小計**                              |      |  **5,400** |
| **太陽電池駆動用**                    |      |            |
| 太陽電池パネル                        |  1   |      2,500 |
| 太陽電池チャージコントローラ          |  1   |      1,500 |
| 完全密封型鉛蓄電池　12V1.2Ah WP1.2-12 |  1   |      1,500 |
| 18650リチウムバッテリー充電モジュール |  1   |        180 |
| **小計**                              |      |  **5,680** |
| **LTE通信用**                         |      |            |
| Nch-MOSFET                            |  1   |        150 |
| DC-DC昇圧コンバータ3V〜5V             |  1   |      1,200 |
| PIX-MT100                             |  1   |     13,000 |
| **小計**                              |      | **14,350** |
| **合計**                              |      | **25,430** |

PIX-MT100が高いので原価としては約25,000円になってしまうが、バッテリー駆動だけなら5,000円程度、太陽電池を入れても、11,000円程度と比較的安価に構築可能である。

## ▼ munin-nodeを作る

muninではmunin-nodeと呼ばれる簡単なTCPサーバを作ると、muninのノードとして登録することができる。電源が供給できる場所に設置するのであれば、電池による間欠駆動などの煩わしいことをしなくてもmuninによる測定データの表示が簡単にできる。

ということで、WiFi経由でmunin-nodeとして動くようにしたのが以下のコードである。

```C++
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiClient.h>

const char *ssid     = "********";
const char *password = "********";
const char *myname   = "esp32";

// Default Munin node port
WiFiServer server(4949);

boolean cmd_done;
boolean args_done;

void setup() {
  Serial.begin(115200);
  // Connect to WiFi network
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Set up mDNS responder:
  if (!MDNS.begin(myname)) {
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  // Start TCP (HTTP) server
  server.begin();
  Serial.println("TCP server started");
  // Add service to MDNS-SD
  MDNS.addService("http", "tcp", 4949);
}

void loop() {
  char buffer[128];
  char c;
  WiFiClient client = server.available();
  if (client) {
    Serial.println("New connection");
    client.print("# munin node at ESP32\n");
    byte pos = 0;
    while (client.connected()) {
      if (client.available()) {
        c = client.read();
        if (c != '\n' && c != '\r') {
          buffer[pos] = c;
          pos += 1;
        } else {
          if (c != '\r') {
            // reset flags on new command
            cmd_done = false;
            args_done = false;
            buffer[pos] = '\0';
            pos = 0;
            String command(buffer);
            Serial.print("Command: ");
            Serial.println(command);
            if (command.startsWith(String("quit"))) {
              client.stop();
              break;
            }
            if (command.startsWith(String("list"))) {
              client.print("temp\n");
              cmd_done = true;
            }
            if (command.startsWith(String("version"))) {
              client.print("munin node at arduino version: 0.1\n");
              cmd_done = true;
            }
            if (command.startsWith(String("config")) && command.endsWith(String("temp"))) {
              client.print("graph_title ESP32 Core 温度\n");
              client.print("graph_args --base 1000 -r\n");
              client.print("graph_vlabel ℃\n");
              client.print("graph_scale no\n");
              client.print("graph_category Measure\n");
              // client.print("graph.info ESP32 Core 温度\n");
              client.print("temp.label ESP32 Core 温度\n");
              client.print("temp.info ESP32\n");
              args_done = true;
              if (! args_done) {
                client.print("# Unknown service\n");
              }
              client.print(".\n");
              cmd_done = true;
            }
            if (command.startsWith(String("fetch")) && command.endsWith(String("temp"))) {
              float temp = temperatureRead();
              client.print("temp.value ");
              if (isnan(temp)) {
                client.print("U");
                Serial.println("Failed to read temperature");
              } else {
                client.print(temp);
                Serial.print("Temperature ");
                Serial.println(temp);
                Serial.println("℃");
              }
              client.print("\n");
              args_done = true;
              if (! args_done) {
                client.print("# Unknown service\n");
              }
              client.print(".\n");
              cmd_done = true;
            }
            if (! cmd_done) {
              client.print("# Unknown command. Try list, config, fetch, version or quit\n");
            }
          }
        }
      }
    }
  }
}
```

とりあえず、esp32のcore温度を返すようにしてあるが、これを拡張すればESP32に接続したセンサー値をネットワーク越しに取得できるようになる。このコードで以下のようなグラフを描画できる。

![esp32 munin-node](./img/esp32-temp-day.png)

これだけではつまらないので、ADT7410温度センサーをつけたものがこれ。

![img_0050](./img/img_0050.jpg)

Wemos Lolin32を使っているのだが、このLolin32はOLEDディスプレイの輝度がだんだん下がってきて終いには全く表示できなくなってしまった。まあこのあたりがチャイナボードの宿命であろう。これで測定した温度が下図。

![adt7410-temp-day](./img/adt7410-temp-day.png)

このようなシステムで２日ほどデータを取得した例[munin](https://sgmail.jp/munin/measure-day.html)の例を示す。**"ADT7410 温度"**が上のLolin32で取得したもの。**"esp32.local > ESP32 Core 温度"**がesp32単体で取得したCPU Coreの温度である。

## ▼ munin-node改

テスト段階ではうまく動いたのだが、途中で止まってしまうという現象が起こった。メインのPCの電源を落とすと止まるようだったのだが、メインのPCとの関連性は不明。原因は推定だが、時間とともにESP32の外部からのアクセスに時間がかかるようになり、muninのタイムアウトを引き起こしているようである。

とりあえず、mDNSでのアクセスをやめて、固定IPでアクセスする、コネクションが終わったらリスタートをかける、といった方策をとったのが以下のコード。

```C++
#include "esp_system.h"
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiClient.h>
#include "FS.h"
#include <SPIFFS.h>

const char *config = "/config.txt";
// Default Munin node port
WiFiServer server(4949);
String ssid;
String passwd;
String myname;

IPAddress localip(192,168,254,32);
IPAddress gateway(192,168,254,1);
IPAddress subnet(255,255,255,0);

boolean cmd_done;

const int wdtTimeout = 20000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;

void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart_noos();
}

void setup() {
  char myid[20];
  Serial.begin(115200);
  if (!SPIFFS.begin()) {
    Serial.println("SPIFFS Mount Failed");
    return;
  }
  delay(100);
  File file = SPIFFS.open(config, "r");
  ssid =   file.readStringUntil('\n');
  passwd = file.readStringUntil('\n');
  myname = file.readStringUntil('\n');
  file.close();
  SPIFFS.end();
  adt7410_init();
  WiFi.begin(ssid.c_str(), passwd.c_str());
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  WiFi.config(localip,gateway,subnet);
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  sprintf(myid,"esp32-%06x",ESP.getEfuseMac());
  if (!MDNS.begin(myid)) {
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  Serial.print("mDNS responder: ");
  Serial.println(myid);
  server.begin();
  Serial.println("TCP server started");
  MDNS.addService("munin", "tcp", 4949);
  // watchdog
  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);                          //enable interrupt
}

int do_quit(WiFiClient &client) {
  client.stop();
  return -1;
}

bool do_list(WiFiClient &client) {
  client.print("temp\n");
  return true;
}

bool do_version(WiFiClient &client) {
  client.print("munin node at arduino version: 0.1\n");
  return true;
}

bool do_config(WiFiClient &client, String arg) {
  if (arg.equals(String("temp"))) {
    client.print("graph_title ADT7410 温度\n");
    client.print("graph_args --base 1000 -r\n");
    client.print("graph_vlabel ℃\n");
    client.print("graph_scale no\n");
    client.print("graph_category Measure\n");
    client.print("temp.label ADT7410 温度\n");
    client.print("temp.info ADT7410\n");
  } else {
    client.print("# Unknown service\n");
  }
  client.print(".\n");
  return true;
}

bool do_fetch(WiFiClient &client, String arg) {
  if (arg.equals(String("temp"))) {
    float temp = adt7410_value();
    client.print("temp.value ");
    if (isnan(temp)) {
      client.print("U");
      Serial.println("Failed to read temperature");
    } else {
      client.print(temp);
      Serial.print("Temperature: ");
      Serial.print(temp);
      Serial.println("℃");
    }
    client.print("\n");
  } else {
    client.print("# Unknown service\n");
  }
  client.print(".\n");
  return true;
}

int do_client(WiFiClient &client) {
  char buffer[128];
  String cmd, arg;
  char c;
  byte pos = 0;
  while (client.connected()) {
    if (client.available()) {
      c = client.read();
      if (c != '\n') {
        if (c == '\r') c = '\0';
        buffer[pos] = c;
        pos += 1;
      } else {
        // reset flags on new command
        cmd_done = false;
        buffer[pos] = '\0';
        pos = 0;
        String cmd(strtok(buffer, " "));
        String arg(strtok(NULL, " "));
        Serial.print("Command: ");
        Serial.print(cmd);
        Serial.print(" ");
        Serial.print(arg);
        Serial.println();
        if (cmd.equals(String("quit"))) {
          do_quit(client);
          break;
        }
        if (cmd.equals(String("list")))    cmd_done = do_list(client);
        if (cmd.equals(String("version"))) cmd_done = do_version(client);
        if (cmd.equals(String("config")))  cmd_done = do_config(client, arg);
        if (cmd.equals(String("fetch")))   cmd_done = do_fetch(client, arg);
        if (! cmd_done) {
          client.print("# Unknown command. Try list, config, fetch, version or quit\n");
        }
      }
    }
  }
}

void loop() {
  timerWrite(timer, 0); //reset timer (feed watchdog)
  WiFiClient client = server.available();
  if (client) {
    Serial.println("New connection");
    client.print("# munin node at ");
    client.print(myname);
    client.print("\n");
    do_client(client);
    ESP.restart();
  }
}
```

また、ssidやpasswordはコードに埋め込みたくなかったので、**SPIFFS**機能(ESP32のフラッフュメモリーの一部をファイルシステムとして使用する機能)を使ってテキストを読み込んで使用するようにした。また、基にしたコードは全ての機能がベタで書いてあるので、それぞれの関数に分割した。

## ▼ munin-probeの作成

ESP32をmunin-node化する試みはやはり安定性が悪く、実用的ではないことがわかったので、HTTPで定期的にnuminサーバに送信するスケッチを作成した。

**munin-probe.ino**

```C++
/*
  munin_probe
*/

#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include "FS.h"
#include <SPIFFS.h>

#define JST (9 * 3600L)

// const char *config = "/config-sd21.txt";
const char *config = "/config-home.txt";
const char *http_server = "https://sgmail.jp/cgi/esp32-node.cgi";
const char *ntp_hosts[] = { "sgmail.jp", "sgmail.jp", "sgmail.jp", NULL };
String ssid, passwd, myname;

// measure data
int measure_coumt = 0;
struct tm timeInfo;
float core_temp;
float adt7410_temp;

void wifi_init() {
  char id[32];
  if (!SPIFFS.begin()) {
    Serial.println("SPIFFS Mount Failed");
    return;
  }
  File file = SPIFFS.open(config, "r");
  ssid =   file.readStringUntil('\n');
  passwd = file.readStringUntil('\n');
  myname = file.readStringUntil('\n');
  Serial.println(ssid);
  Serial.println(passwd);
  Serial.println(myname);
  sprintf(id, "-%06x", ESP.getEfuseMac());
  myname += id;
  file.close();
  SPIFFS.end();
  WiFi.begin(ssid.c_str(), passwd.c_str());
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void ntp_init() {
  if (true) {
    configTime(JST, 0, ntp_hosts[0], ntp_hosts[1], ntp_hosts[2]);
    getLocalTime(&timeInfo);
    Serial.printf("NTP date : %04d-%02d-%02d %02d:%02d:%02d\n",
                  timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    if ((timeInfo.tm_year + 1900) < 1971) {
      Serial.print("Can not NTP sync, force restart\n");
      delay(1 * 1000);
      ESP.restart();
    }
  } else {
    configTime(JST, 0, NULL, NULL, NULL);
  }
}

void measure() {
  getLocalTime(&timeInfo);
  core_temp = temperatureRead();
  adt7410_temp = adt7410_value();
  measure_coumt ++;
}

void http_send() {
  HTTPClient http;
  int httpCode;
  char buffer[256];
  Serial.print("http send start\n");
  sprintf(buffer,
          "%s?node=%s&date=%04d-%02d-%02dT%02d:%02d:%02d&"
          "core_temp=%f&adt7410_temp=%f",
          http_server, myname.c_str(),
          timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec,
          core_temp,
          adt7410_temp
         );
  Serial.printf("%s\n", buffer);
  for (int i = 0; i < 5; i ++) {
    http.begin(buffer);
    http.setTimeout(10 * 1000);
    httpCode = http.GET();
    if (httpCode > 0) {
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        Serial.println("Payload : " + payload);
        http.end();
        return;
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      if (httpCode == HTTPC_ERROR_READ_TIMEOUT) {
        http.end();
        return;
      }
    }
    http.end();
    Serial.print("http send retry after 10 sec.\n");
    delay(1 * 10000);
  }
}

void setup() {
  Serial.begin(115200);
  wifi_init();
  ntp_init();
  adt7410_init();
}

void loop() {
  measure();
  http_send();
  Serial.println("sleep to next time");
  delay(2.5 * 60 * 1000);
  if(measure_coumt > 24) {
    ESP.restart();
  }
}
```

このスケッチは２分３０秒(muninのスケジュールの５分の半分)毎にmuninサーバに測定データを送るようにしている。muninサーバ側では、

**esp32-node.cgi**

```ruby
#!/usr/bin/ruby

require 'cgi'

cgi = CGI.new()
node = cgi.params["node"][0]
if(node)
  temp = File.dirname(__FILE__) + "/data/" + node + ".tmp"
  data = File.dirname(__FILE__) + "/data/" + node + ".dat"
  File.open(temp,"w") {|fd|
    cgi.params.each {|k,v|
      fd.puts "#{k}:#{v.join(",")}"
    }
  }
  File.rename(temp,data)
  cgi.out({"type"=>"text/plain","connection"=>"close"}) { "OK\r\n" }
end
#`./esp32-mail.sh`
```

でデータを受信、保存し、

**esp32-node.rb**

```ruby
#!/usr/bin/ruby

if(ARGV[0])
  data = File.dirname(__FILE__) + "/data/" + ARGV[0] + ".dat"
  ARGV.shift
  v = "NaN"
  begin
    File.open(data,"r") {|fd|
      while(line = fd.gets()) do
        ARGV.each {|k|
          if(line =~ /^#{k}:(.*?)$/)
            v = $1
          end
        }
      end
    }
  rescue
  end
  puts v
end
```

のようなスクリプトでデータを抽出し、

**esp32_adt7410_temp**

```shell
#!/bin/bash

if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi
if [ "$1" = "config" ]; then
  echo 'graph_title ESP32 ADT7410 温度'
  echo 'graph_args --base 1000 -r'
  echo 'graph_vlabel ℃'
  echo 'graph_scale no'
  echo 'graph_category Measure'
  echo 'graph_info ESP32 ADT7410 温度'
  echo 'temp.label ESP32 ADT7410 温度'
  echo 'temp.info ESP32'
  exit 0
fi
echo -n "temp.value "
/var/www/cgi/esp32-node.rb esp32-24a4ae30 adt7410_temp
```

のようなmuninプラグインでデータをmuninサーバに引き渡す。

![esp32_adt7410_temp-day](./img/esp32_adt7410_temp-day.png)

その結果のグラフがこれ。

## ▼ muninのグラフを見やすく

muninはプラグインを作ればすぐにグラフを表示することができ非常に便利なのだが、自動的に生成されるグラフに手を入れようとすると、かなり厄介である。が、できなくはないので、やってみたのが下のグラフである。

![bme280_temp-day](./img/bme280_temp-day.png)

１時間に１回のデータ取得だと、どうしてもグラフがギザギザになってしまったり、たまにデータ抜けが起こるので、少しでも見やすくしようと**移動平均**を取ることで、グラフの平滑化を図ったものである。ギザギザの線よりは少し見やすくなっているものと思う。同時に25℃に基準線も追加してある。

## ▼ 抵抗用丸ピンソケット

前に、抵抗用に丸ピンソケットを加工したものを示した。その時はピンをはんだ付けしていたが、プラスチックのベースからピンを抜いて差し替えることができることに気がついた。そこで作ったのがこれ。

![img_0051](./img/img_0051.jpg)

このピンを使うと、

![img_0052](./img/img_0052.jpg)

このようにブレッドボードに根元まで刺すことができ部品のグラつきが少なくなる。

## ▼ 0SIM

測定データをLTE経由でサーバに送るために、PIX-MT100と[0SIM](http://mobile.nuro.jp/0sim/)を組み合わせて使用しているが、サーバに接続できずに測定値が抜けてしまうことがある。どうも500MBまで無料であるがゆえであると思うが(回線が細いのかルータが非力なのかわからないが)、頻発するようであれば他の格安SIMに乗り換える必要があるかもしれない。

もう少し詳しく書くと、2時～3時台、5時～7時台、12時台、20時台あたりが通しにくい時間帯になっている。必ず通信できないというわけではないので、LTE通信の混雑が原因の可能性が高い。サーバ側は、自宅のサーバとさくらインターネット上のサーバの両方を試してみたが、有意な違いはなかった。

まあ、このようなことを勘案すると、LTE通信の混雑が主原因であろうと思われるわけである。

## ▼ OLEDディスプレイいろいろ

![img_0053](./img/img_0053.jpg)

いくつか評価用として購入したOLEDディスプレイである。左から、

- [KKHMF 0.95 SPI インチカラー OLED モニタモジュール For Arduino](https://www.amazon.co.jp/gp/product/B01M8JCPYX/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
  SPI接続のフルカラーOLEDディスプレイ。解像度は、96x94である。

- [HiLetgo 0.96" I2C IIC シリアル 128×64 OLED LCDディスプレイ SSD1306液晶 STM32/51/MSP430/Arduinoに対応 ブルー](https://www.amazon.co.jp/gp/product/B07122PG12/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
  I2C接続のモノクロOLEDディスプレイ。解像度は192x64。

- [HiLetgo 0.91" IIC I2C シリアルOLED液晶ディスプレイモジュール 128x32 3.3V/5V AVR PIC Arduino UNO MEGA Arduinoに対応](https://www.amazon.co.jp/gp/product/B01M8JV310/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
  同じくI2C接続のモノクロディスプレイであるが、解像度は192x32と非常に小さい。コンパクトなので、フリスクケースや小さな筐体に収容する場合には重宝しそうである。

これらを使った具体的な用途として、常時表示可能な電流・電圧計などを考えているが、OLEDの寿命が短そうなことを考慮すると、スイッチを押したときだけ表示するような工夫が必要ではないかと考えている。

![img_0054](./img/img_0054.jpg)

表示例。**u8g2**というライブラリを使用して表示した。漢字も表示できる。

![img_0059](./img/img_0059.jpg)

これも、u8g2を使用して表示した。複数のフォントが使用できる。写真では白っぽく見えるが実際の表示はブルー（というかシアン？）である。

