[TOC]

# ■ 参考URL
----

以下に参考にしたURLを列挙する。

- [Raspberry Pi](http://www.raspberrypi.org/)
- [RSオンライン](http://jp.rs-online.com/web/home.html)
- [KSY](https://raspberry-pi.ksyic.com/)
- [スイッチサイエンス](https://www.switch-science.com/)
- [slee-Pi 2](https://www.switch-science.com/catalog/3276/)
- [秋月電子通商](http://akizukidenshi.com/catalog/)
- [ストロベリー・リナックス](https://strawberry-linux.com/catalog/)
- [ASUS Tinker Board | Physical Computing Lab](http://www.physical-computing.jp/product/1300)
- [RPi1114 - Raspberry Pi 電源制御モジュール](https://jiwashin.blogspot.jp/p/rpi-1114.html)
- [ＡＤＴ７４１０使用　高精度・高分解能　Ｉ２Ｃ・１６Ｂｉｔ　温度センサモジュール： センサ一般 秋月電子通商 電子部品 ネット通販](http://akizukidenshi.com/catalog/g/gM-06675/)
- [Amazon.co.jp： 5v dc dc コンバータ - Amazonプライム対象商品](https://www.amazon.co.jp/s/ref=sr_nr_p_76_0?fst=as%3Aoff&amp;rh=i%3Aaps%2Ck%3A5v+dc+dc+%E3%82%B3%E3%83%B3%E3%83%90%E3%83%BC%E3%82%BF%2Cp_76%3A2227292051&amp;keywords=5v+dc+dc+%E3%82%B3%E3%83%B3%E3%83%90%E3%83%BC%E3%82%BF&amp;ie=UTF8&amp;qid=1500089961&amp;rnid=2227291051)
- [USB出力型ソーラーパネルとモバイルバッテリーでIoT機器をオフグリッド化する方法 - おばかさんよね。](http://obakasanyo.net/off-grid-usb-iot/)
- [コラム：藤山哲人の実践! 家電ラボ 第16回：1万円でスマホが充電できる太陽光発電システムを自作しよう!-家電Watch](http://kaden.watch.impress.co.jp/docs/column/fujilabo/568196.html)
- [2016年の物欲その11「INA226 I2Cデジタル電流・電圧・電力計モジュール」を使ってRaspberry Piで測定してみた /usePocket.com別館](http://usepocket.com/c/UP552.html)
- [ドウジンテイスウ.log — Raspberry pi からINA219を使っていろいろ測定](http://t.doujin-constant.net/post/147804641889/raspberry-pi-%E3%81%8B%E3%82%89ina219%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%A6%E3%81%84%E3%82%8D%E3%81%84%E3%82%8D%E6%B8%AC%E5%AE%9A)
- [Raspberry Pi ピン接続](http://pi4j.com/pins/model-2b-rev1.html)
- [ラズベリーパイ屋外稼動キット「Pi-field」 - メカトラックス株式会社｜IoT/M2M機器/Raspberry Pi用周辺機器](https://mechatrax.com/products/pi-field/)
- [Rapsberry pi 用 ミニUPS - ＋galileo7＋ Arduinoシールドの輸入・オリジナル製品の販売](http://www.galileo-7.com/?pid=98460387)
- [I2C温度センサー(ADT7410)を使う | Make.](http://make.bcde.jp/raspberry-pi/i2c%E6%B8%A9%E5%BA%A6%E3%82%BB%E3%83%B3%E3%82%B5%E3%83%BCadt7410%E3%82%92%E4%BD%BF%E3%81%86/)
- [wittyPi - ＋galileo7＋ Arduinoシールドの輸入・オリジナル製品の販売](http://www.galileo-7.com/?pid=91881362)
- [タブレイン、Raspberry Piで3G通信モジュールが利用できるPi Zeroサイズの拡張ボード「3GIM HAT」を発表 | fabcross](https://fabcross.jp/news/2016/04/20160427_tabrain_3gimhat.html)
- [LoRa Hat for Raspberry Pi Zero from jose_navarro on Tindie](https://www.tindie.com/products/jose_navarro/lora-hat-for-raspberry-pi-zero/)
- [Raspberry Pi ZeroをUSBケーブル1本で遊ぶ | Japanese Raspberry Pi Users Group](http://www.raspi.jp/2016/07/pizero-usb-otg/)
- [Raspberry Piの無線LANアクセスポイント化 - DesignAssembler](http://hyottokoaloha.hatenablog.com/entry/2015/02/24/000655)
- [TinkerOSに日本語環境を設定する](https://signal-flag-z.blogspot.jp/2017/08/tinkerosjapanese.html)
- [パナソニックのモバイルバッテリー搭載ＡＣ急速充電器はIoT機器のUPSとして使えるかも | きっと何かに役立つでしょ！？](https://kitto-yakudatsu.com/archives/2669)
- [逆電圧保護](http://www.geocities.jp/neofine9/work/revblk/revblk.html)
- [muninの監視間隔を変更する | CATch a TAIL!](https://blog.necomimi.net/?p=1786)
- [ｐｃＤｕｉｎｏ　Ｌｉｎｕｘ　Ｄｅｖ　Ｂｏａｒｄ： マイコン関連 秋月電子通商 電子部品 ネット通販](http://akizukidenshi.com/catalog/g/gM-06931/)
- [How to use PhoenixSuit to Flash New Image to pcDuino3 | LinkSprite Learning Center](http://learn.linksprite.com/pcduino/quick-start/pcduino3/how-to-use-phoenixsuit-to-flash-new-image-to-pcduino3/)
- [LinkSprite pcDuino1 – LinkSprite](http://www.linksprite.com/linksprite-pcduino1/)
