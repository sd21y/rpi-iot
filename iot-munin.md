[TOC]

# ■ 計測データの可視化
----

## ▼ 計測データの可視化の方法

IoTデバイスで計測したデータは多くの場合、グラフ化するなどして可視化することが求められる。
可視化するには、

1. **csv**データを読み込んで、**excel**もしくは**openoffice**などでグラフ化
2. **munin**,**cacti**,**zabbix**などのwebの可視化ツールを使ってグラフ化

などの方法がある。

(1)の方法は自由度が大きい分手間がかかる。(2)の方法は閲覧には手間がかからない代わり、設定に手間が掛かるなど、長所・短所があるので、目的に応じて使い分ける必要がある。ここでは、muninを使った可視化を実際にやってみたものを例示する。

- [トップページの例](https://a-shigi.bitbucket.io/munin-index.html)
- [計測データの例](https://a-shigi.bitbucket.io/munin-measure.html)
- [VPSサーバの例](https://a-shigi.bitbucket.io/munin-vps.html)

また、実際のリアルタイムのグラフは、

- [リアルタイムグラフ](http://rodeo.mydns.jp/munin/)

で確認できる。

## ▼ muninを使用した可視化

基本的にmuninはサーバ監視用なので、デフォルトではサーバの動作状況を監視するためのプラグインが設定されているが、必要なプラグインを作成すれば、気温や湿度などのデータもグラフ化することができる。実際に温度計を接続し、グラフ化した例を以下に示す。

**adt7410での計測例**

![munin-adt7410-day.png](./img/munin-adt7410-day.png)

このグラフは部屋においてあるペルチェ冷蔵庫の庫内温度を測定したものである。このグラフを作成するために使用したプラグイン及び温度計測用のスクリプトを以下に示す。

**/etc/munin/plugins/adt7410**
```shell
#!/bin/bash
if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi
if [ "$1" = "config" ]; then
  echo 'graph_title adt7410'
  echo 'graph_args --base 1000 -l 0'
  echo 'graph_vlabel Celsius'
  echo 'graph_scale no'
  echo 'graph_category Measure'
  echo 'temp.label child box'
  echo 'graph_info child box'
  echo 'temp.info /root/adt7410.py'
  # Last, if run with the "config"-parameter, quit here (don't
  # display any data)
  exit 0
fi
echo -n "temp.value "
/root/adt7410.py
```

**/root/adt7410.py**
```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import smbus
import time

i2c = smbus.SMBus(1)
address = 0x48

block = i2c.read_i2c_block_data(address, 0x00, 12)
temp = (block[0] << 8 | block[1]) >> 3
if(temp >= 4096):
    temp -= 8192
print("%6.2f" % (temp / 16.0),flush=True)
```

また、以前購入していたUSB接続の温度計[**Temper 1F**](https://www.amazon.com/Thermometer-Temperature-Sensor-Computer-TEMPer/dp/B01LZLBGEX)を使用して室内の温度を測定した例を以下に示す。

**Temper 1Fでの計測例**

![munin-temper-day.png](./munin-temper-day.png)

**/etc/munin/plugins/temper**
```shell
#!/bin/bash
if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi
if [ "$1" = "config" ]; then
  echo 'graph_title Room Temperature'
  echo 'graph_args --base 1000 -l 0'
  echo 'graph_vlabel Celsius'
  echo 'graph_scale no'
  echo 'graph_category Measure'
  echo 'temp.label MyRoom'
  echo 'graph_info Room Temperature'
  echo 'temp.info /usr/local/bin/temper-poll'
  # Last, if run with the "config"-parameter, quit here (don't
  # display any data)
  exit 0
fi
echo -n "temp.value "
/usr/local/bin/temper-poll -c
```

**/usr/local/bin/temper-pollのメインプログラム**
```python
# encoding: utf-8
from __future__ import print_function, absolute_import
import argparse
import logging
from .temper import TemperHandler

def parse_args():
    descr = "Temperature data from a TEMPer v1.2 sensor."
    parser = argparse.ArgumentParser(description=descr)
    parser.add_argument("-p", "--disp_ports", action='store_true',
                        help="Display ports")
    units = parser.add_mutually_exclusive_group(required=False)
    units.add_argument("-c", "--celsius", action='store_true',
                       help="Quiet: just degrees celcius as decimal")
    units.add_argument("-f", "--fahrenheit", action='store_true',
                       help="Quiet: just degrees fahrenheit as decimal")
    units.add_argument("-H", "--humidity", action='store_true',
                       help="Quiet: just percentage relative humidity as decimal")
    parser.add_argument("-s", "--sensor_ids", choices=['0', '1', 'all'],
                        help="IDs of sensors to use on the device " +
                        "(multisensor devices only)", default='0')
    parser.add_argument("-S", "--sensor_count", type=int,
                        help="Override auto-detected number of sensors on the device")
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    quiet = args.celsius or args.fahrenheit or args.humidity

    logging.basicConfig(level = logging.ERROR if quiet else logging.WARNING)

    th = TemperHandler()
    devs = th.get_devices()
    if not quiet:
        print("Found %i devices" % len(devs))

    readings = []

    for dev in devs:
        if args.sensor_count is not None:
            # Override auto-detection from args
            dev.set_sensor_count(int(args.sensor_count))

        if args.sensor_ids == 'all':
            sensors = range(dev.get_sensor_count())
        else:
            sensors = [int(args.sensor_ids)]

        temperatures = dev.get_temperatures(sensors=sensors)
        humidities = dev.get_humidity(sensors=sensors)
        combinations = {}
        for k, v in temperatures.items():
            c = v.copy()
            try:
                c.update(humidities[k])
            except:
                pass
            combinations[k] = c
        readings.append(combinations)

    for i, reading in enumerate(readings):
        output = ''
        if quiet:
            if args.celsius:
                dict_key = 'temperature_c'
            elif args.fahrenheit:
                dict_key = 'temperature_f'
            elif args.humidity:
                dict_key = 'humidity_pc'

            for sensor in sorted(reading):
                output += '%0.1f; ' % reading[sensor][dict_key]
            output = output[0:len(output) - 2]
        else:
            portinfo = ''
            tempinfo = ''
            huminfo = ''
            for sensor in sorted(reading):
                if args.disp_ports and portinfo == '':
                    portinfo = " (bus %(bus)s - port %(ports)s)" % reading[sensor]
                try:
                    tempinfo += '%0.1f°C %0.1f°F; ' % (
                        reading[sensor]['temperature_c'],
                        reading[sensor]['temperature_f'],
                    )
                except:
                    pass
                try:
                    huminfo += '%0.1f%%RH; ' % (reading[sensor]['humidity_pc'])
                except:
                    pass
            tempinfo = tempinfo[0:len(output) - 2]
            huminfo = huminfo[0:len(output) - 2]

            output = 'Device #%i%s: %s %s' % (i, portinfo, tempinfo, huminfo)
        print(output)

if __name__ == '__main__':
    main()
```

**temper**の計測に関しては、**C言語**で書かれた計測用プログラムもあるのだが、**temper 1F**で使用すると、分解能が1℃になってしまい、使い物にならなかったので、**python**で書かれた計測用プログラムを使用することにした。

**munin**を使うとこのように比較的簡単に可視化が可能であるが**munin**が使用している**rrdtool**のバージョンによってはグラフのラベルなどに漢字を使用すると文字化けを起こすことがあるため、ラベルには英語表記を使用したほうが良い。

と書いたのだが、**Raspbian GNU/Linux 9.1 (stretch)**にアップデートしたらグラフ内の文字化けは起こらなくなった。ただ、HTMLテキスト部分は文字化け(文字コードの不一致)を起こすので`/etc/munin/templates/partial/head.tmpl`を次のように変更すると正常に漢字も表示できるようになる。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <link rel="stylesheet" href="<TMPL_VAR NAME="R_PATH">/static/style-new.css" type="text/css" />
  <TMPL_IF NAME="SHOW_ZOOM_JS">
     <script src="<TMPL_VAR NAME="R_PATH">/static/formatdate.js" type="text/javascript" language="javascript"/>
     <script src="<TMPL_VAR NAME="R_PATH">/static/querystring.js" type="text/javascript" language="javascript"/>
     <script src="<TMPL_VAR NAME="R_PATH">/static/zoom.js" type="text/javascript" language="javascript"/>
  </TMPL_IF>
  <TMPL_UNLESS NAME="SHOW_ZOOM_JS">
    <meta http-equiv="refresh" content="300" />
  </TMPL_UNLESS>
  <title><TMPL_LOOP NAME="PATH"><TMPL_IF NAME="pathname"> :: <TMPL_VAR ESCAPE="HTML" NAME="pathname"><TMPL_ELSE>Munin</TMPL_IF></TMPL_LOOP></title>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
  <meta name="author" content="Auto-generated by Munin" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="<TMPL_VAR NAME="R_PATH">/static/favicon.ico" />
  <link rel="SHORTCUT ICON" href="<TMPL_VAR NAME="R_PATH">/static/favicon.ico"/>
</head>
```

## ▼ **munin**を使用する上での問題点

このように、**munin**を使うと比較的簡単に測定データをグラフ化できるのだが、今回のシステムとはあまり相性が良くない。

というのも、**munin**はマスターからノードに定期的にデータを要求してrrdデータベースに格納してグラフの描画を行うのだが、本システムは一時間に数分しか起動していないため、**munin**が本システムのノードにデータを要求しても大部分の時間シャットダウンされているので、データが取れないということになってしまう。

仕方がないので、数分の稼働時間を少し伸ばして一時間に一度は**munin**からのデータ要求に応えられるように設定してみたが、どうもうまくいかない。タイミングは合っているはずだが、全くデータが取得できないのである。

そこで、考え方を変えて、本システムが測定を行ったタイミングで、データを**munin**サーバに送信しておき、**munin**マスタからのデータ要求はこのデータを処理することとした。**munin**のサンプリング間隔はデフォルトでは5分なので、データは5分遅れで反映されることになる。こうすることで測定データだけはなんとか取れるようになるはずである。

## ▼ **Microsoft edge**で**munin**が見えない

とんでもないことに気がついた。どうやら**Microsoft edge**は**mDNS**による名前解決ができないようだ。これだと、**munin**と同じネットワークで**edge**をメインに使っている人は**munin**のグラフを見ることができない。**chrome**や**firefox**を使えばなんの問題もないが、それを強制するのもいかがなものかと思う。

それに、日本のネットワーク環境はほとんど**NAT**を使ってインターネットに接続しているわけだが、この場合、ネットワーク内部からのアクセスと外部からのアクセスは、異なる**URL**を使わなければならない。本実験の環境だと、内部からは[http://pi3.local/munin/](http://pi3.local/munin/)でアクセスし、外部からは[http://rodeo.mydns.jp/munin/](http://rodeo.mydns.jp/munin/)でアクセスしないと正常に**munin**にアクセス出来ない。

これは、**munin**に限った話ではなく、ローカルネットワークにおいたサーバを外部から参照したりする場合に必ず発生する問題である。サーバのアクセスはローカル環境からだけに限定してしまえば問題はないが、今時、**iPhone**からアクセスしたいといった要求はかならずあるので、内部と外部でURLが異なるのはやや使い勝手が悪いと思う。

そこで考えたのが、ローカルネットワークの外部に、アクセス用のリーバスプロクシサーバを立てて、内部からも外部からもこのサーバを使うというものである。幸い、データ収集用に`rodeo.sgmail.jp`サーバがあるので、これを流用することにした。アクセスできるようにするための手順を以下に示す。

### 1. pi3.localを外部からアクセスできるようにする

これは、**pi3.local**のアドレスを固定アドレスにして、ルータのNATの設定で**静的NAT**の設定を行い、ルータの**80番ポート**にアクセスされたら**pi3.local**に転送する設定にすればよい。

### 2. ルータのアドレスを固定アドレスにするか名前で参照できるようにする

基本的なローカルサーバの公開方法である、固定アドレスをルータに付与する方法か、ダイナミックDNSを使った名前解決を行うことでローカルサーバにインターネットからアクセスできるようにする。今回は、`rodeo.mydns.jp`でアクセスできるようにダイナミックDNSの登録を行った。

### 3. リバースプロクシを立てる

**rodeo.sgmail.jp**には既に**apache2**が立ててあるので、
```shell
sudo a2enmod proxy_http
```
でリバースプロクシを使えるようにして、`/etc/apache2/mods-enabled/proxy.conf`を次のように変更する。
```shell
<IfModule mod_proxy.c>
        ProxyRequests Off
        <Proxy *>
                # SSLでの接続のみ許可する。
                # 「sudo a2enmod ssl」でSSLを有効化して設定をしておくこと。
                SSLRequireSSL
                Order deny,allow
                Allow from all
                #Basic認証の設定
                #AuthType Basic
                #AuthName "foo"
                #AuthUserFile /etc/apache2/mods-enabled/users.passwd
                #Require valid-user
        </Proxy>
        ProxyPass        /munin/ http://rodeo.mydns.jp/munin/
        ProxyPassReverse /munin/ http://rodeo.mydns.jp/munin/
        ProxyPass        /munin-cgi/ http://rodeo.mydns.jp/munin-cgi/
        ProxyPassReverse /munin-cgi/ http://rodeo.mydns.jp/munin-cgi/
</IfModule>
```
これで、[https://sgmail.jp/munin/](https://sgmail.jp/munin/)にアクセスすると[http://pi3.local/munin/](http://pi3.local/munin/)にアクセスすることができる。

非常にまどろっこしいが、どこからでもローカルネットワーク内部のサーバにアクセスするには、このような方法を取らざるをえない。

`https`でアクセスしているのは、このサーバは以前、[Let'sencrypt](https://letsencrypt.org/)を使った**ssl**サーバのテストに使用しているためである。

## ▼ xxx.localが使えるのはiTunes(avahi)のおかげだった

めでたしめでたしと思っていたら、私のwindows10からmDNSでアクセスが可能なのはiTunesを入れているためであることが判明した。素の状態ではmDNSでアクセスすることができないことが判明した。MacやLinux(raspbian)は素の状態でmDNSに対応しているので、Windowsだけなんとかする必要がある。まあ、上記の対応を行えば関係ないとも言えるが、管理のため個々の**Raspberry pi**にアクセスする際にはmDNSが使えたほうが断然便利である。

そこで、[ダウンロード - Bonjour Print Services (Windows)](https://support.apple.com/kb/DL999?locale=ja_JP)をインストールすることを推奨する。これをインストールすれば、mDNSが使えるようになる。本来はbonjour経由でプリンタを使うためのサポートソフトであるが、iTunesをインストールするよりは軽いので、こちらを使うことを薦める。もちろん、iTunesがインストールされているなら何もする必要はない。

## ▼ muninのY軸スケールの調整

**munin**は便利なツールなのだが、**Y軸**の目盛りのとり方が自動もしくは半自動で設定できる項目が少なく、データによっては目盛線がほとんど描画されず、わかりにくい表示になってしまう。

気圧をグラフにする場合、

**気圧のグラフ**

![気圧のグラフ](./img/bme280_pressure-week.png)

のように、Y軸には**1000hPa**の軸以外描画されない。これだと高低はある程度判別できるが実際の値は読み取りにくい。なんとか調整したいのだが、実際の測定値をそのまま使って調整する方法は無いようだ。

仕方がないので測定データから**1000hPa**引いた値を使用して描画することで、それなりにY軸の軸線が表示されるはずである。

また、電圧の表示は通常の表示はOKなのだが、表示を拡大するモードではY軸に同じ値の目盛線が表示されてしまう。これは軸目盛りが整数で表示されるためで、データを詳細に見ようとした場合に非常に困る。

これも仕方がないので、データを**1000倍**した値で描画することとし、**Y軸**の単位を**mV**とすることで、細かな電位差を読み取れるようにした。

**電圧のグラフ**

![電圧のグラフ](./img/bme280_voltage-pinpoint=1505506235,1505614235.png)

これで、Y軸の値をきちんと読めるようになった。

このように、muninは手軽だが癖があるのでグラフを表示するには工夫が必要になる場合がある。

