#!/bin/bash
LANG=C
DATE=`date +'%a, %d %b %Y %H:%M %z'`
sleep 30
/usr/sbin/sendmail -t <<EOS
From: a.shigi@gmail.com
To: shigi8086@gmail.com
Date: ${DATE}
Subject: ${HOSTNAME} Start battery drive
Content-Type: text/plain; charset="UTF-8"

バッテリー駆動を開始します。

`./battery.sh`
EOS
