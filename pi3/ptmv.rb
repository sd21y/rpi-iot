#!/usr/bin/ruby

File.open("/root/ptmv.txt","r") {|fd|
  line = fd.gets()
  if(line =~ /^.*?&p=([\d\.]+)&t=([\d\.]+)&m=([\d\.]+)&v=([\d\.]+)$/)
    p = $1.to_f + 72.1 - 1000.0
    t = $2.to_f
    m = $3.to_f
    v = $4.to_f * 1000.0
    case ARGV[0]
    when "p" then puts p
    when "t" then puts t
    when "m" then puts m
    when "v" then puts v
    else
      puts "#{p},#{t},#{m},#{v}"
    end
  else
    puts "NaN"
  end
}
