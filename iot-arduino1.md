[TOC]

# ■ IoTにおけるArduino その１
----

[電源系](./iot-power.md)でも書いたが、**Arduino**は単体ではできることが限られているが、**Raspberry pi**と組み合わせたり、高機能な周辺機器と組み合わせることで、様々な利用方法が考えられる。ここでは、その応用方法について考察してみたい。

## ▼ Arduinoとは

まとまった情報としては[WikipediaのArduinoの項目](https://www.wikiwand.com/ja/Arduino)が参考になる。また["arduino"でググれば](https://www.google.co.jp/search?q=arduino&ie=utf-8&oe=utf-8&hl=ja)様々な情報が引っかかってくる。情報は非常に多いので、かえってその中から有用な情報を見つけるのが大変な状況にある。

価格もいわゆる**ピンキリ**で、高いものは**5,000円以上**するものもあれば、中華ボードならば**400円**くらいからとなかなかの幅がある。今回評価に使用したのは「[Mini USB Arduino Nano V3.0 改造バージョン Arduino Nano V3.0互換ボード ATmega328P搭載 (5個)](https://www.amazon.co.jp/gp/product/B01F741W6O/ref=oh_aui_detailpage_o07_s00?ie=UTF8&psc=1)」というもので、5個で1,980円である。中華ボードの特徴としてはオンボードのUSBシリアル変換チップが**CH340**に変更されていてる、USBコネクタも**mini-B規格**になっているなど、本物に比べて多少の変更点がある。

**Arduino**はほとんど**ワンチップマイコン**であり、周辺チップとしては**USBシリアル変換チップ**と**12V→5Vシリーズレギュレータ**が載っている程度である。なので量産する場合はCPUチップのみ購入して互換ボードと同等の回路を構成することができる。

**Arduino**は高度な処理を行うことはできず、インターネットに接続することもTCPパケットの送受信程度の低機能な部分に限られており、そのままではIoTデバイスとしてはあまり使いやすいものではない。

しかしながら、**Raspberry pi zero W**と組み合わせると、補完的な動作をさせることが可能になり、活用の幅が大きく広がると思われる。

## ▼ Arduino microを入手した

**Arduino**は結構高いなあ、と思い躊躇していたのだが、**Arduino micro**が**2,780円**と比較的手頃な価格だったので購入してみた。

![Arduino micro](./img/img_0131.jpg)

見ての通り、**34PIN DIP**パッケージと非常に小さい。
ブレッドボードに刺すことができるので、実験には便利な構造になっている。

なぜ購入したかというと、きちんとした理由がある。

- 電源をいれるだけで所定のプログラムが動作する
- 電源電圧が、7～12Vとワイド
- RTCモジュールが追加できる
- RTCモジュールからArduinoに割り込みがかけられる
- sleepモードが複数あり、数mAまで消費電力を落とすことができる

つまり、**RPi1114FDH**より安価に低消費電力の電源管理モジュールとして使える可能性があるのだ(まあ、やってみないとわからないが)。

とりあえず、RTCモジュールの接続や電源管理用のMOS-FETを接続しての**Lチカ**などの予備的な実験を行う予定である。

## ▼ Arduino nano互換品を入手した

とりあえず、Arduino microでの**Lチカ**はできたので「[Mini USB Arduino Nano V3.0 改造バージョン Arduino Nano V3.0互換ボード ATmega328P搭載 (5個)](https://www.amazon.co.jp/gp/product/B01F741W6O/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)」なるものを入手した。**５個で1,980円**と格安の中華ボードである。
本物の**Arduino nano**はスイッチサイエンスで、**１個2,880円**なので**Arduino micro**と大差のない価格であるが、中華ボードは格安なので、幾つか壊したとしても惜しくはない。電源及びI2Cの実験用に使用する予定である。

**中華ボード**
![中華ボード](./img/img_0133.jpg)

これも、テストとして**Lチカ**で動作の確認を行った。

## ▼ I2Cの接続方法

最終的に**I2C**で**Raspberry pi**と**Arduino**を接続してマスタ・スレーブ構成のシステムにするわけだが、ひとつ問題が発覚した。それは、**I2C**の電圧が**Raspberry pi**は**3.3V**なのに対して**Arduino**は**5V**だということ。ロジックレベルの電圧が違うので、何らかの対策を行わなければならない。

そこで「[I2Cバス用双方向電圧レベル変換モジュール(PCA9306)](http://akizukidenshi.com/catalog/g/gM-05452/)」を使用して**I2C**のレベル変換を行って接続するようにする。価格も**１個150円**と安いので、このような目的には気軽に使える。

と目論んでいたのだが、あとで述べるセミ・セルフプログラミング環境を構築する場合、**Raspberry pi**と**Arduino**はシリアル(USBシリアル)で接続するのが最も合理的であると思うようになってきたのと、**I2C**は**Arduino**にデバイスを接続するために使用したいとか、いろいろあって**I2C**を使って接続することは断念した。

が、やってみたら、シリアル回線を使って**Arduino**を接続すると、**スケッチ**を書き込んだり、ターミナルソフトを使って**Arduino**に接続すると、**Arduino**が**リセット**されてしまうことがわかった。まあ確かに、何らかの方法でリセットがかからないと困るわけだが、ターミナルソフトで接続しただけでリセットされてしまうのは、ちょっと困ったことになる。詳細については後で述べる。

## ▼ Raspberry pi + Arduino

**Raspberry pi zero W**と**Arduino Nano**を組み合わせたのが下の写真である。

**Raspberry pi + Arduino**
![Raspberry pi + Arduino](./img/img_0134.jpg)

単純にケーブルを繋いだだけではあるが、この構成に多少の部品をつけると、

- **Raspberry pi zero W**の間欠駆動用コントローラ
- **Arduino**のセミ・セルフプログラミング環境
- **Arduino**を計測用プローブにする

などの用途に使用することができる。このときは比較的長い、通常のUSBケーブルを使用したが、現在は、「[USB mini-B→micro-B接続ケーブル](https://www.amazon.co.jp/gp/product/B00O4VKNXU/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)」を使って、変換コネクタなどなしにダイレクトに接続している。

### Raspberry pi Wの間欠駆動用コントローラ

**Raspberry pi zero W**を間欠的に駆動するために**RPi1114FDH**使用してきたわけだが、この役割を**Arduino**で行うことが可能であり、その動作もセミ・セルフプログラミング環境を使って**Raspberry pi zero W**から設定や変更を行うことができる。

**Arduino nano**の場合、動作している状態の消費電力は**10mA**程度であり、RTCモジュールと適当なスリープモードを選択すれば**数mA以下**にすることも不可能ではない。そうなると一旦は却下した太陽電池なしのバッテリー駆動も再度実現性を帯びてくる。

と思っていたのだが、先に述べたターミナルソフトで**リセット**がかかる問題をなんとかしないと、**Raspberry pi**から**Arduino**をコントロールしようとした途端、**RAspberry pi**の電源が落ちる、といったことが起こりかねないのである。

### Arduinoのセミ・セルフプログラミング環境

**Arduino**のプログラム開発には[Arduino IDE](https://www.arduino.cc/en/main/software)を使用して行うことが一般的であるが、この開発環境は**Raspberry pi zero W**上でも動かすことができる。USBケーブルが繋がっていれば、立派な開発環境として使用することが可能である。

**Raspberry pi zero W**で**X11**ウインドウ環境を動かすのはメモリーも食うし、リモートで行うことができないということが問題になるかもしれない。その場合には、[PlatfoemIO](http://platformio.org/)という開発環境をインストールすれば、Linuxのコマンドラインから**Arduino**プログラムのコンパイル・インストールを行うことができる。

### Arduinoを計測用プローブにする

**Raspberry pi zero W**を間欠的に動かす場合、**Raspberry pi zero W**単体では間欠動作のタイミングでしか計測を行うことができない。しかし**Arduino**をスレーブとして使えば、計測自体は**Arduino**のメモリーが許す限りの頻度で行うことができる。

## ▼ Piduino構想

とりあえず、電源系を含めた**Raspberry pi zero W**と**Arduino nano**をどのように接続するかを示したのが下図である。**構想**というほどだいそれたものではないのだが、回路にはちょっとした工夫がある。

![Piduino](./img/pi-duino.png)

付加した回路は電源系の**DC-DCコンバータ**と制御用の**p-ch MOSFET**だけである。**MOSFET**は**p-ch**タイプを使用することで、**Arduino**の電源が**OFF**の場合に**ON**になるようになっている。
こうすることで、シリアル回線を繋いだときに**Arduino**がリセットされたときでも**Raspberry pi**の電源が**OFF**にならないようにしている。**Raspberry pi**の電源を**OFF**にするときは**Arduino**の**D2**ピンを**HIGH**にすれば良い。

とりあえず、予備的な実験として**Raspberry pi**の代わりに**LED**をつけて正常に動くかどうかを検証した回路が下の写真である。

![検証回路](./img/img_0135.jpg)

**D2**ピンの代わりに**D13**ピンに接続して**Lチカ**プログラムを走らせたもの。LEDが交互に点滅し正常に動作していることが確かめられた。また、**D2**ピンに接続して、リセットされた時もLEDが点灯状態のままであることを検証した。またLEDに印加される電圧を測定し、**VCC電圧**がそのままLEDに印加されていることも検証した。

## ▼ リアルタイムクロック(RTC)について

**Raspberry pi**だけを使っている場合にはあまり意識しないが、**Raspberry pi**も**Arduino**も基本的にはパソコンなどでは標準で付いている**リアルタイムクロック**が付いていない。

**Raspberry pi**であまり問題にならないのはリアルタイムクロックの代わりに**NTP**サーバにアクセスして時刻を取得しているためである。当然ながら**NTP**にアクセスするにはインターネットにつながっている必要があり、そのような機能がないか、限られている**Arduino**では別途何らかの方策が必要になる。

そのために使われているのが、

- [VKLSVAN DS3231 AT24C32 IIC 高精度リアルタイムクロックモジュールArduino用](https://www.amazon.co.jp/gp/product/B01J5F5ORO/ref=oh_aui_detailpage_o01_s01?ie=UTF8&psc=1)
- [リアルタイムクロック(RTC)モジュール DS1307 EEPROM:AT24C32(32Kb)搭載](https://www.amazon.co.jp/gp/product/B00P51G026/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)

といった**RTC**と呼ばれるモジュールである。要するに単なる時計なのだが、**I2C**バスに接続して使用することができ、**DS3231**を使用したものは**Arduino**にタイマー割り込みを掛けることができるようになっている。

何らかの計測を行う場合、**時間**は非常に重要なファクタであり、時間が不正確では、測定データ自体が無意味になることすらある。またこれらの**RTC**にはバックアップ電池が搭載できるようになっており、**Arduino**の電源がOFFの場合にも時刻を保持することができる。

問題は、どうやって最初の時刻合わせを行うかだ。インターフェースは**I2C**しか無いわけだから、**I2C**経由で最初の時刻を設定しなければならないのだが、いちいち手入力で時刻合わせなど行いたくはない。

そこで、次のようなスケッチを使って時刻合わせを行うのが良いようだ。スケッチとは**Arduino**のプログラムのことで、言語仕様としては**C/C++言語**の組み込み版で`main()`からではなく、`setup()`と`loop()`関数があり、`setup()`は最初に一度だけ実行され、`loop()`は`setup()`の後に呼び出される。`loop()`は無限に呼び出されるようになっていて、プログラムを終了する方法はない。

```c
// CONNECTIONS:
// DS3231 SDA --> SDA
// DS3231 SCL --> SCL
// DS3231 VCC --> 3.3v or 5v
// DS3231 GND --> GND

/* for software wire use below
#include <SoftwareWire.h>  // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>

SoftwareWire myWire(SDA, SCL);
RtcDS3231<SoftwareWire> Rtc(myWire);
 for software wire use above */

/* for normal hardware wire use below */
#include <Wire.h> // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);
/* for normal hardware wire use above */

void setup () {
    Serial.begin(9600);
    Serial.print("compiled: ");
    Serial.print(__DATE__);
    Serial.println(__TIME__);
    //--------RTC SETUP ------------
    // if you are using ESP-01 then uncomment the line below to reset the pins to
    // the available pins for SDA, SCL
    // Wire.begin(0, 2); // due to limited pins, use pin 0 and 2 for SDA, SCL
    Rtc.Begin();
    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__) + 18;
    printDateTime(compiled);
    Serial.println();
    if (!Rtc.IsDateTimeValid()) {
        // Common Cuases:
        //    1) first time you ran and the device wasn't running yet
        //    2) the battery on the device is low or even missing
        Serial.println("RTC lost confidence in the DateTime!");
        // following line sets the RTC to the date & time this sketch was compiled
        // it will also reset the valid flag internally unless the Rtc device is
        // having an issue
        Rtc.SetDateTime(compiled);
    }
    if(!Rtc.GetIsRunning()) {
        Serial.println("RTC was not actively running, starting now");
        Rtc.SetIsRunning(true);
    }
    RtcDateTime now = Rtc.GetDateTime();
    if(now < compiled) {
        Serial.println("RTC is older than compile time!  (Updating DateTime)");
        Rtc.SetDateTime(compiled);
    }
    else if (now > compiled) {
        Serial.println("RTC is newer than compile time. (this is expected)");
    }
    else if (now == compiled) {
        Serial.println("RTC is the same as compile time! (not expected but all is fine)");
    }
    // never assume the Rtc was last configured by you, so
    // just clear them to your needed state
    Rtc.Enable32kHzPin(false);
    Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}

void loop () {
    if (!Rtc.IsDateTimeValid()) {
        // Common Cuases:
        //    1) the battery on the device is low or even missing and the power line was disconnected
        Serial.println("RTC lost confidence in the DateTime!");
    }
    RtcDateTime now = Rtc.GetDateTime();
    printDateTime(now);
    Serial.println();
    RtcTemperature temp = Rtc.GetTemperature();
    Serial.print(temp.AsFloat());
    Serial.println("C");
    delay(10000); // ten seconds
}

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt) {
    char datestring[20];
    snprintf_P(datestring,
            countof(datestring),
            PSTR("%04u/%02u/%02u %02u:%02u:%02u"),
            dt.Year(),
            dt.Month(),
            dt.Day(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    Serial.print(datestring);
}
```

`__DATE__`と`__TIME__`マクロを使用するのが**ミソ**である。このマクロは、スケッチのコンパイル時点の日付と時刻が入っていて、それぞれ、`"Sep 28 2017"`,`"17:18:52"`のような文字列に展開される。スケッチではこの文字列を日付、時刻として評価し、RTCの日付・時刻を設定する。コンパイル時点の日付・時刻(これはNTPで取得した時刻なので比較的正確)
なので、実際にプログラムを書き込んで動くまでの時間分ずれるので、その分をの秒数を加算して設定している。このスケッチを使うと２秒程度の誤差内で時刻の設定を行うことができる。

**Arduino**の言語については「[Arduino 日本語リファレンス](http://www.musashinodenpa.com/arduino/ref/)」があり、標準でどのような機能が使えるかがわかる。ただし特定のハードウエアを前提にしたものもあるので、全ての機能が最初から使えるわけではない。

上の、**RTC**で使われているライブラリも上記リファレンスには含まれていないので、ライブラリの解説やソースコードを見て使用方法を見つける必要がある。

上のスケッチで日付・時刻を合わせたら、次のスケッチをコンパイル・書き込みを行うと、一分に一回タイマー割り込みを行ってシリアルコンソールに日付時刻を表示する。

```c
//Reference :
//https://github.com/JChristensen/DS3232RTC
//http://forum.arduino.cc/index.php?topic=109062.0
//http://donalmorrissey.blogspot.jp/2010/04/sleeping-arduino-part-5-wake-up-via.html
//http://easylabo.com/2015/04/arduino/8357/

#include <DS3232RTC.h>    //http://github.com/JChristensen/DS3232RTC
#include <Time.h>         //http://www.arduino.cc/playground/Code/Time
#include <Wire.h>         //http://arduino.cc/en/Reference/Wire (included with Arduino IDE)
#include <avr/sleep.h>
#include <avr/power.h>

bool rtcint = false;

void setup(void) {
  //clear all
  RTC.alarmInterrupt(1, false);
  RTC.alarmInterrupt(2, false);
  RTC.oscStopped(true);

  //sync time with RTC module
  Serial.begin(9600);
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  if(timeStatus() != timeSet) {
    Serial.println("Unable to sync with the RTC");
  } else {
    Serial.println("RTC has set the system time");
  }
  Serial.flush();

  //show current time, sync with 0 second
  digitalClockDisplay();
  synctozero();

  //set alarm to fire every minute
  RTC.alarm(2);
  attachInterrupt(1, alcall, FALLING);
  RTC.setAlarm(ALM2_EVERY_MINUTE , 0, 0, 0);
  RTC.alarmInterrupt(2, true);
  digitalClockDisplay();
}

void loop(void) {
  //process clock display and clear interrupt flag as needed
  if (rtcint) {
    rtcint = false;
    digitalClockDisplay();
    RTC.alarm(2);
  }

  //go to power save mode
  enterSleep();
}

void synctozero() {
  //wait until second reaches 0
  while (second() != 0) {
    delay(100);
  }
}

void alcall() {
  //per minute interrupt call
  rtcint = true;
}

void digitalClockDisplay(void) {
  // digital clock display of the time
  setSyncProvider(RTC.get); //sync time with RTC
  Serial.print(year());  Serial.print('/');
  printDigits(month());  Serial.print('/');
  printDigits(day());    Serial.print(' ');
  printDigits(hour());   Serial.print(':');
  printDigits(minute()); Serial.print(':');
  printDigits(second());
  Serial.println();
  Serial.flush();
}

void printDigits(int digits) {
  // utility function for digital clock display: prints preceding colon and leading 0
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void enterSleep(void) {
  //enter sleep mode to save power
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_mode();
  sleep_disable();
  power_all_enable();
}
```

このように、複数のスケッチを使い分けたりすることがあるので、**Raspberry pi**と**Arduino**が常時接続されていることは非常に重要になってくるのである。もし、常時接続でないとしたら、別の**Arduino**を使って時刻合わせを行った**RTC**を**外して**ターゲットの**Arduino**に接続するといった方策が必要になってしまう。

## ▼ GR-CITRUS & WA-MIKAN 入手

いろいろ情報をあさっていたら、[GR-CITRUS](http://akizukidenshi.com/catalog/g/gK-10281/)なるものを見つけた。なんと、マイコン上で**RUBYVM**が標準で動いていて**mruby/c**(組み込み向けruby処理系)でプログラムが作れる、ということで、価格も**2,400円**と比較的リーズナブルで、CPUも**Arduino**に比べると**96MHz**と高速。[WA-MIKAN](http://akizukidenshi.com/catalog/g/gK-10530/)(**1,500円**)と組み合わせるとWi-Fiも使えるという触れ込み。ピン配列は**Arduino Pro Mini**互換ということで**Arduino**の周辺デバイスが利用可能である。これは評価するしか無い、ということで早速ポチってしまった。

で先日ブツが届いたので早速評価してみた。

**上がGR-CITRUS下がWA-MIKAN**
![GR-CITRUS+WA-MIKAN](./img/img_0136.jpg)

使ってみた結果は、残念な結果であった(;_;)。

- mrubyを使ったときは**割り込み**が使えない。
- 消費電力が100mA以上と大きい。
- mruby実行時は**deep_sleep**などの省電力機能が使えない。
- **RTC**が付いているがバッテリーバックアップがない。
- 開発環境が**Arduino**互換の割味は微妙に使いにくい。
- そのままでは**PC**に接続しないと動作しない。
- 電源だけで動かすにはジャンパをショートさせる必要がある。
- ジャンパは**ハーフピッチ**(1.27mm)のものが必要。
- **ライブラリ**が揃っていない。

いろいろ書いたけど、mrubyが動くのは結構良いアプローチだと思うので、太陽電池で動かそうという目的にはあまり適していないが、目的によっては十分使える可能性があるので、もう少し使う方を考えてみたい。

## ▼ ESP8266の可能性

**GR-CITRUS**は少し残念な結果だったが、**WA-MIKAN**で使われている**ESP8266**は太陽電池駆動に非常に有用な機能を持っているので、少し深掘することにした。
このチップは、**Wi-Fi**アクセスと基本的な**TCPサーバ・クライアント**としての機能を持ち、**単独でArduino**として動作させることが可能である。

「[WROOM-02で温度／湿度計測　その１](http://okiraku-camera.tokyo/blog/?p=4738)」というブログによると、**Arduino IDE**を使ってプログラミング可能であり、いろいろ制限はあるものの、待機時には**deep_sleep**が使用可能で消費電力をかなり低く抑えることが可能なようだ。

また、**WA-MIKAN**自体も「[Arduino IDEでWA-MIKAN(和みかん)のESP8266をプログラミングする　環境インストール編](https://qiita.com/tarosay/items/28ba9e0208f41cec492d)」にあるように少し面倒だが、同様のプログラムを動作させることができるようだ。まあ価格を考えると積極的に**WA-MIKAN**を使うことはありえないのだが。

ということで、**WA-MIKAN**を流用するにしろ、新たに**WROOM-02**を使うにしろ**3.3V**に対応した**USBシリアル変換モジュール**は必要になるので、これまた購入することにした。購入したのは、

- [Wi-Fiモジュール ESP-WROOM-02 DIP化キット](http://akizukidenshi.com/catalog/g/gK-09758/)
- [FT231X USBシリアル変換モジュール](http://akizukidenshi.com/catalog/g/gK-06894/)

である(うーん、ブログのまんまだよなぁ)。これだけあれば、実際にESP-WROOM-02を使ってWi-Fi経由で測定を行うことが可能だ。

**ESP-WROOM-02とFT231X**
![ESP-WROOM-02とFT231X](./img/img_0138.jpg)

いつものようにamazonで安い中華ボードがないかどうか調べていたら、

**HiLetgo ESP8266 NodeMCU LUA CP2102 ESP-12E モノのインターネット開発ボード ESP8266 無線受信発信モジュール WIFI無ジュール Arduinoに適用 （2個セット） [並行輸入品]**
![HiLetgo ESP8266 NodeMCU LUA CP2102 ESP-12E モノのインターネット開発ボード ESP8266 無線受信発信モジュール WIFI無ジュール Arduinoに適用 （2個セット） [並行輸入品]](./img/img_0140.jpg)

と言うものを見つけてポチッた。ポチったときはあまり気にしていなかったのだが、商品名の中の「**NodeMCU LUA**」というのがなかなかの曲者であった。これについては後述する。

これで低消費電力で測定ができるようであれば、**N-MOSFET**を使った電源スイッチを付加して**PIX-MT100**の電源もコントロールすることができれば完璧である。と目論見道理になるかどうかはやってみなけりゃわからないので、とりあえずやってみる。

## ▼ 「NodeMCU LUA」とは何か？

ポチったときには気にしていなかった「**NodeMCU LUA**」とは何か？調べてみたら、意外ととんでもないものであった。「**NodeMCU**」でググると、

- [ESP8266](https://www.wikiwand.com/ja/ESP8266)
- [ESP8266にNodeMCUをアップロードしてHello Worldする](https://qiita.com/masato/items/a91e0df84ac8be45e305)
- [ESP8266 上で Lua インタプリタが動くファームウェア、NodeMCU を使う](http://embedded.hateblo.jp/entry/2016/09/16/192333)

といった情報が引っかかってくる。**nodeMCU**は**ESP8266**上で動作している**luaVM**であり、**luaスクリプト**で書かれたプログラムを動作させる事ができる。**GR-CITRUS**の上で**mrubyスクリプト**が動くのと同じようなものである。**mruby**と違う点は、

- 言語仕様がmrubyよりも軽そうである
- deep_sleepのようなハードよりのライブラリがある
- Wi-Fi,htttp,mDNS,sntpなど通信関係のライブラリが豊富にある
- 用途に適したカスタムVMが作成・ダウンロードできるサイトがある

など、mrubyよりも開発環境は整備されていると言えそうだ。ただ、日本では**luaインタプリタ**はあまり普及しているとは言い難く、日本語で検索できた情報には「**Hello World**」程度で終わっており、情報が多いとは言い難い(まあmrubyも豊富とは言い難いのだが)。

また、開発環境の多くが**python**で書かれ、**linux**上で開発することを前提としているものが多く、windowsでの情報が少ないため、mruby同様、実際に開発を行うには超えなければならないハードルが多く存在する。しかしながら、**Arduino IDE**のような完成度の開発環境が出てくれば、相当使えそうな感じである。

## ▼ ESP8266でWebサーバを動かす

Amazonで買ったボードは**nodeMCU**がプリインストールされているというだけで、物は**ESP8266**モジュールを積んだワンボードマイコンである。なので、当然**Arduino IDE**を使ってプログラムを作って動かすことができる。そこで、次のようなスケッチを書き込んで動かしてみた。

```C++
/*
  ESP8266 mDNS responder sample

  This is an example of an HTTP server that is accessible
  via http://esp8266.local URL thanks to mDNS responder.

  Instructions:
  - Update WiFi SSID and password as necessary.
  - Flash the sketch to the ESP8266 board
  - Install host software:
    - For Linux, install Avahi (http://avahi.org/).
    - For Windows, install Bonjour (http://www.apple.com/support/bonjour/).
    - For Mac OSX and iOS support is built in through Bonjour already.
  - Point your browser to http://esp8266.local, you should see a response.

 */


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>

#define LED 16

const char* ssid = "<ssid>";
const char* password = "<password>";

// TCP server at port 80 will respond to HTTP requests
WiFiServer server(80);

void setup(void) {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  Serial.begin(115200);
  // Connect to WiFi network
  WiFi.begin(ssid, password);
  Serial.println("");  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  // Set up mDNS responder:
  // - first argument is the domain name, in this example
  //   the fully-qualified domain name is "esp8266.local"
  // - second argument is the IP address to advertise
  //   we send our IP address on the WiFi network
  if(!MDNS.begin("esp8266")) {
    Serial.println("Error setting up MDNS responder!");
    while(1) { 
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  // Start TCP (HTTP) server
  server.begin();
  Serial.println("TCP server started");
  // Add service to MDNS-SD
  MDNS.addService("http","tcp",80);
}

void loop(void) {
  // Check if a client has connected
  WiFiClient client = server.available();
  if(!client) {
    return;
  }
  Serial.println("");
  Serial.println("New client");
  // Wait for data from client to become available
  while(client.connected() && !client.available()) {
    delay(1);
  }
  // Read the first line of HTTP request
  String req = client.readStringUntil('\r');
  // First line of HTTP request looks like "GET /path HTTP/1.1"
  // Retrieve the "/path" part by finding the spaces
  int addr_start = req.indexOf(' ');
  int addr_end = req.indexOf(' ', addr_start + 1);
  if (addr_start == -1 || addr_end == -1) {
    Serial.print("Invalid request: ");
    Serial.println(req);
    return;
  }
  digitalWrite(LED, LOW);
  req = req.substring(addr_start + 1, addr_end);
  Serial.print("Request: ");
  Serial.println(req);
  client.flush();
  String s;
  if(req == "/") {
    IPAddress ip = WiFi.localIP();
    String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
    s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>Hello from ESP8266 at ";
    s += ipStr;
    s += "</html>\r\n\r\n";
    Serial.println("Sending 200");
  } else {
    s = "HTTP/1.1 404 Not Found\r\n\r\n";
    Serial.println("Sending 404");
  }
  client.print(s);
  digitalWrite(LED, HIGH);
  Serial.println("Done with client");
}
```

Webサーバとして動くのはもちろんだが、**mDNS**も一緒に動かしているので、IPアドレス指定ではなく、

```
http://esp2866.local/
```

のように名前でアクセスすることができる。この時の消費電力を測定してみると、**80mA**から**90mA**程度と常時電源が入っている状態としては低消費電力だといえる。

また、ping応答を見てみると、
```DOS
C:\Users\ashig>ping esp8266.local

esp8266.local [192.168.254.182]に ping を送信しています 32 バイトのデータ:
192.168.254.182 からの応答: バイト数 =32 時間 =2ms TTL=128
192.168.254.182 からの応答: バイト数 =32 時間 =3ms TTL=128
192.168.254.182 からの応答: バイト数 =32 時間 =24ms TTL=128
192.168.254.182 からの応答: バイト数 =32 時間 =1ms TTL=128

192.168.254.182 の ping 統計:
    パケット数: 送信 = 4、受信 = 4、損失 = 0 (0% の損失)、
ラウンド トリップの概算時間 (ミリ秒):
    最小 = 1ms、最大 = 24ms、平均 = 7ms

C:\Users\ashig>
```
のように、Wi-Fi経由でも数msのラウンドトリップタイムで結構早い印象である。

## ▼ ESP8266でWeb電圧計を作る

単純にwebサーバを作っただけではつまらないので、これに**I2C**の電流・電圧・電力計を接続してweb上で測定できるようにした。

![web電流・電圧・電力計](./img/img_0141.jpg)

[http://esp8266.local/](http://esp8266.local/)にアクセスすると、

```text
Web-meter by ESP8266 at 192.168.254.182
電圧: 0.00mV
電流: 0.00mA
電力: 0.00mW
```

のようなページを表示する。ここでは入力端子に何も接続していないので、全て**0.00**であるが、電源を接続すれば、その電圧や電流を表示する。詳細は[ソースコード](https://bitbucket.org/sd21y/rpi-iot/src/110be0aa9b2d50a7e8702c3544c806a23b3c41c2/Arduino/web-meter/?at=master)を見てほしい。

## ▼ ESP8266の省電力動作

![省電力テストボード](./img/img_0145.jpg)

上の写真のような簡単な回路でESP8266の**deep_sleep**のテストを行った。

回路的には**D0**ピンと**RST**ピンを**1KΩ**の抵抗を介して接続しているところがミソである。このような回路にすると、**deep_sleep**が使えるようになり、**deep_sleep**中は内臓のリアルタイムクロック以外の電源が落とされ、論理的には数μAレベルの消費電力になる。実際にはUSB周りの回路が生きていたりするため、数mAレベルだと思われるが、それでも定常動作時の100mAレベルに比べれば、1/10以下の消費電力に抑えることができる。また、USBからの電源供給ではなく、**Vin**ピンもしくは**3V3**ピンから電源を供給するようにすれば、もう少し論理値に近い消費電力にすることが可能ではないかと思われる。**deep_sleep**ができることは、次のような**sketch**で動作の確認を行った。

```c++
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>

// LED port
#define LED_PIN D8
#define RST_PIN 16

// pre-wakeup time
#define LTE_WAKEUP 60
#define PRE_WAKEUP 20
// HTTP
const char* httpServerIP = "192.168.254.7";
// Wi-Fi
const char* wifi_ssid   = "<ssid>";
const char* wifi_passwd = "<password>";
// NTP
unsigned int localPort = 2390;      // local port to listen for UDP packets
const char* ntpServerName = "ntp.nict.jp";
IPAddress timeServerIP;            // time.nist.gov NTP server address
const int NTP_PACKET_SIZE = 48;      // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP udp;                         // A UDP instance to let us send and receive packets over UDP

void WiFiconnect(void) {
int count = 0;
  Serial.print("Connecting Wi-Fi ");
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid,wifi_passwd);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    count ++;
    if(count > 20) {
      pinMode(RST_PIN,OUTPUT);
      digitalWrite(RST_PIN,LOW);
    }
  }
  Serial.println();
  Serial.print("Connected to ");
  Serial.println(wifi_ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

unsigned long sendNTPpacket(IPAddress& address) {
  Serial.println("Sending NTP packet ...");
  memset(packetBuffer,0,NTP_PACKET_SIZE); // set all bytes in the buffer to 0
  // Initialize values needed to form NTP request
  packetBuffer[ 0] = 0b11100011;           // LI, Version, Mode
  packetBuffer[ 1] = 0;                    // Stratum, or type of clock
  packetBuffer[ 2] = 6;                    // Polling Interval
  packetBuffer[ 3] = 0xEC;                 // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123);          // NTP requests are to port 123
  udp.write(packetBuffer,NTP_PACKET_SIZE);
  udp.endPacket();
}

time_t getNTPtime() {
  udp.begin(localPort);
  Serial.print("UDP local port: ");
  Serial.println(udp.localPort());
  WiFi.hostByName(ntpServerName,timeServerIP); 
  sendNTPpacket(timeServerIP);   // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no packet yet");
    return(0);
  } else {
    Serial.print("Packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = " );
    Serial.println(secsSince1900);
    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);
    epoch += 3600 * 9; // Add JST offset
    return(epoch);
  }
}

time_t getNTP() {
unsigned long epoch = 0;
  while(epoch == 0) {
    epoch = getNTPtime();
    if(epoch != 0) break;
    delay(1000);
    Serial.println("Retry ...");
  }
  return(epoch);
}

char* mysPrintf(char* buf,int len,char *fmt, ...) {
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,len,fmt,args);
  va_end(args);
  return(buf);
}

void myPrintf(char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,128,fmt,args);
  va_end(args);
  Serial.print(buf);
}

void myPrintfln(char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,128,fmt,args);
  va_end(args);
  Serial.println(buf);
}

void measure(time_t left) {
HTTPClient http;
char buffer[256];
int httpCode;
  mysPrintf(buffer,256,"http://%s/cgi/esp8266.cgi?t=%04d/%02d/%02dT%02d:%02d:%02d&l=%d",httpServerIP,year(),month(),day(),hour(),minute(),second(),left);
  http.begin(buffer);
  Serial.printf("HTTP GET request: %s\n",buffer);
  httpCode = http.GET();
  if(httpCode > 0) {
    // HTTP header has been send and Server response header has been handled
    Serial.printf("HTTP GET response: %d\n",httpCode);
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      Serial.println(payload);
    }
  } else {
    Serial.printf("HTTP GET faiLED_PIN, error: %s\n",http.errorToString(httpCode).c_str());
  }
  http.end();
}

void setup() {
unsigned long start,measure_time,sleep_time,error_time;
time_t left;
  // wakeup indicator
  pinMode(LED_PIN,OUTPUT);
  digitalWrite(LED_PIN,HIGH);
  //デバッグ用にシリアルを開く
  Serial.begin(115200);
  Serial.println();
  Serial.print("Reboot reason: ");
  Serial.println(ESP.getResetReason());
  // Wait for LTE dongle starting
  Serial.println("Wait LTE dongle starting ...");
  delay(LTE_WAKEUP * 1000);
  // Start
  WiFiconnect();
  setSyncProvider(getNTP);
  setSyncInterval(300);
  myPrintfln("Wakeup: %04d/%02d/%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),second());
  // measure(0);
  // xx:00:00まで待機
  left = (hour() + 1) * 60 * 60 - ((hour() * 60 * 60) + (minute() * 60) + second());
  if(left > (PRE_WAKEUP + LTE_WAKEUP)) {
    myPrintfln("Short deep sleep %d sec ...",left - (PRE_WAKEUP + LTE_WAKEUP));
    digitalWrite(LED_PIN,LOW);
    ESP.deepSleep((left - (PRE_WAKEUP + LTE_WAKEUP)) * 1000 * 1000,WAKE_RF_DEFAULT);
    delay(1000); // deepsleepモード移行までのダミー命令
  } else {
    myPrintf("Waiting %d sec at %02d:00:00 ...",left,hour());
    while(minute() != 0 || second() != 0) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
  }
  start = millis();
  myPrintfln("Measure start: %04d/%02d/%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),second());
  measure(left);
  measure_time = (millis() - start) / 1000;
  myPrintfln("Measure time: %d sec ...",measure_time);
  digitalWrite(LED_PIN,LOW);
  myPrintfln("Measure end: %04d/%02d/%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),second());
  sleep_time = 60 * 60 - (measure_time + PRE_WAKEUP + LTE_WAKEUP);
  myPrintfln("Powerdown deep sleep %d sec ...",sleep_time);
  ESP.deepSleep(sleep_time * 1000 * 1000,WAKE_RF_DEFAULT);
  delay(1000); // deepsleepモード移行までのダミー命令
}

void loop() {}
```

この**sketch**では大まかに、

- LTEドングルの起動待ち(60秒)
- Wi-Fi接続を開始する
- NTPサーバから時刻を取得し、Timerに設定
- xx:00:00までdeep_sleepまたはループで待機
- 測定(今は何もしていない)
- 次のタイミングを計算し、deep_sleepを実行

ということを行っている。

**deep_sleep**は、指定の時間が経過するまで省電力モードになり、指定の時刻になったら**D0**ピンを**LOW**にする。**D0**ピンは**RST**ピンにつながれているので**ESP8266**が**リセット**される、という**かなり乱暴な**スリープモードである。

```text
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718315739
Unix time = 1509326939
Wakeup: 2017/10/30 10:28:59
Short deep sleep 1791 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718317444
Unix time = 1509328644
Wakeup: 2017/10/30 10:57:24
Short deep sleep 86 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718317586
Unix time = 1509328786
Wakeup: 2017/10/30 10:59:46
Waiting 14 sec at 10:00:00 ...............................
Measure start: 2017/10/30 11:00:00
HTTP GET request: http://192.168.254.7/cgi/esp8266.cgi?t=2017/10/30T11:00:00&l=14
HTTP GET response: 200
DONE
Measure time: 0 sec ...
Measure end: 2017/10/30 11:00:00
Powerdown deep sleep 3530 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi .....................
<...文字化け...>
Reboot reason: External System
Wait LTE dongle starting ...
Connecting Wi-Fi .....................
Reboot reason: External System
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718321041
Unix time = 1509332241
Wakeup: 2017/10/30 11:57:21
Short deep sleep 89 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718321186
Unix time = 1509332386
Wakeup: 2017/10/30 11:59:46
Waiting 14 sec at 11:00:00 ...............................
Measure start: 2017/10/30 12:00:00
HTTP GET request: http://192.168.254.7/cgi/esp8266.cgi?t=2017/10/30T12:00:00&l=14
HTTP GET response: 200
DONE
Measure time: 0 sec ...
Measure end: 2017/10/30 12:00:00
Powerdown deep sleep 3530 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718324486
Unix time = 1509335686
Wakeup: 2017/10/30 12:54:46
Short deep sleep 244 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718324775
Unix time = 1509335975
Wakeup: 2017/10/30 12:59:35
Waiting 25 sec at 12:00:00 .....................................................
Measure start: 2017/10/30 13:00:00
HTTP GET request: http://192.168.254.7/cgi/esp8266.cgi?t=2017/10/30T13:00:00&l=25
HTTP GET response: 200
DONE
Measure time: 0 sec ...
Measure end: 2017/10/30 13:00:00
Powerdown deep sleep 3530 sec ...
```

実際に動かすと上記のように59分(3540秒)スリープするはずが、54～55分程度の時間経過すると起きてしまい、時間調整用の短時間の**deep_sleep**が実行されるため、実際の計測を行うために２回**deep_sleep(=リセット)**が実行されてしまう。消費電力的にはあまりよくないのだが、deep_sleepの精度が悪いので、この方法が一番安定して動作する可能性がある。

複雑なOSがないシステムの利点はこのように強制的なリセットを行っても大丈夫なところにある。**Raspberry Pi**ではこれを安全に行うために色々工夫はしているが、起動途中で電源が落ちるというような事態になると**SDカード**が破損したり、最悪ボード自体が壊れる可能性がある。Arduinoではそのような事態になる心配は少ない。

この回路に、ニッケル水素電池x4の電源を**Vin**に供給して**INA226**を使った電流・電圧計で測定したところ、

| 条件      | 状態       | Vin電圧 | Vin電流 |
| ------- | -------- | ----- | ----- |
| USB接続あり | Wi-Fi通信時 | 5.57V | 80mA  |
| USB接続あり | 待機時      | 5.57V | 10mA  |
| USB接続なし | Wi-Fi通信時 | 5.57V | 70mA  |
| USB接続なし | 待機時      | 5.57V | 3mA   |

という結果になった。論理値のμAレベルにはならなかったものの、USB接続なしの状態では待機時の電流が3mAとかなり省電力になっていることがわかる。待機時の電流・電圧を次に示す。

![待機時の消費電流](./img/img_0146.jpg)

どうやら**NodeMCU**を使った構成での低消費電力化はこのあたりが限界のようだ。

![img_0147](./img/img_0147.jpg)

ちなみに、0.9Vから3.3Vに昇圧可能なDC-DCコンバータを接続してニッケル水素電池２本で動かしてみたのが上の写真である。この状態でどのくらい電池が持つかを現在実験中である。

で、ニッケル水素電池でだいたい３日くらいは動作するようだ。こんなもんかと思いつつ、シリアル接続用の回路が違えば、多少消費電力も変わるのではないかと思い、別の[ESR8266ボード](https://www.switch-science.com/catalog/2500/)でも試してみたのだが、

```text
Web-meter by ESP8266
MyIP: 192.168.254.182
日時: 2017/11/08 17:32:53
気温: DS18B20: 27.13℃,RTC: 27.75℃
電圧: 8101.25mV
電流: 4.00mA
電力: 50.00mW
```

ということで、全く同じ消費電力であった。これは別のWeb上で電圧・電流・消費電力を測定できるように作成した、ESP8266ボードで測定した。

## ▼ 電圧・電流・消費電力計

**NodeMCU**の電池駆動では消費電力の削減が重要であるが、この時、地味に役に立ったのが、**Arduino nano**に**LCD**(I2C接続のもの)をつけて作った電圧・電流・消費電力計である。上の写真の表示がそうなのだが、最初は単なる時計だったのだが、それに**INA226**を使った電圧・電流・電力を表示できるようにしたものだ。消費電力を常時モニタしたいとなれば、テスターではとても無理だし、最近の安物のテスターは電流が計測できなかったりするので、まずひとつ、このようなものを作っておくと動作検証がしやすくなる。

また、Web上で見ることができるものも作成しておいたので、上記のスイッチサイエンスのボードの消費電力測定に使用した。これもこういった動作確認には便利である。

## ▼ ESP32

ESP8266はリーズナブルで使いやすいのだが、その後継機として[ESP32](https://www.wikiwand.com/ja/ESP32)というチップが出ている。機能的にもかなり強化され、クロックも160MHzもしくは240MHzと高速になっている(低消費電力の観点からするとオーバースペックのような気もするが)。とりあえず、ESP8266同様、[開発ボード](https://www.amazon.co.jp/HiLetgo-ESP32-ESP-32S-NodeMCU%E9%96%8B%E7%99%BA%E3%83%9C%E3%83%BC%E3%83%892-4GHz-Bluetooth%E3%83%87%E3%83%A5%E3%82%A2%E3%83%AB%E3%83%A2%E3%83%BC%E3%83%89/dp/B0718T232Z/ref=pd_)が売られているので入手してみた。実は、現在販売されているESP8266の大部分は[技適](https://www.wikiwand.com/ja/%E6%8A%80%E9%81%A9%E3%83%9E%E3%83%BC%E3%82%AF)を受けていないが、ESP32は技適を受けているので、違法かどうかを気にする必要がない。技適自体、Wi-Fiではほとんど形式的なものになっていて非関税障壁と化しているものの、技適マークがないと形式的には**違法**である。

## ▼ ブレッドボード

![ブレッドボード](./img/img20171111_000046.jpg)

説明していなかったが、こういった実験にはブレッドボードが便利でよく使っている。上の写真のようにブレッドボードは穴が金属のコネクタでつながっていて、同じ列にピンを刺すと電気的に接続できるようになっている。なので、間違って同じコネクタの列に電源などを接続してしまうと、簡単にショートしてしまうので、回路を構成する際にはよく注意して接続しなければならない。

## ▼ 抵抗

![抵抗その１](./img/img20171111_000025.jpg)

抵抗をブレッドボードに刺す場合、足が長くかつ細いため、扱いに困る場合が多い。何かいい方法がないかと思い考えたのがこれ。圧着端子に抵抗を付けてしまえば、抜き差しもしやすく縦型になるので場所も取らない。最大の欠点は作るのが面倒であることだ。あと何かラベルを貼るなりしておかないと抵抗値がわかりにくい。もっといい方法はないものか思案中である。

![抵抗その２](./img/img_0022.jpg)

最初の方法だと、作ってしまうと抵抗の交換ができないので使い回しができない。そこで、一列タイプのICソケットを２ピンずつ切り離して使う方法を考えた。ただ、普通に売られているソケットはピンの長さが短く、ブレッドボードの端子まで届かない。そこで、ICソケットと同じタイプのピンだけ売っていたので、それを継ぎ足してブレッドボードに刺せるようにした。この方法だと、抵抗の交換が簡単にできるので必要な抵抗を付け替えて使用できる。

## ▼ NodeMCU-32Sを使った測定回路

**ブレッドボード構成**

![esp32ブレッドボード](./img/img_0016.jpg)

**回路図**

![esp32回路図](./img/nodeMCU32S.jpg)

[NodeMCU-32S](https://www.amazon.co.jp/HiLetgo-ESP32-ESP-32S-NodeMCU%E9%96%8B%E7%99%BA%E3%83%9C%E3%83%BC%E3%83%892-4GHz-Bluetooth%E3%83%87%E3%83%A5%E3%82%A2%E3%83%AB%E3%83%A2%E3%83%BC%E3%83%89/dp/B0718T232Z)を使った測定回路である。[本体・計測系](./iot-device.md)の**Raspberry -pi zero W**を使ったものと同等の測定を**NodeMCU-32S**で実現しようというものである。

消費電力を削減するため、NodeMCU-32Sは降圧型DC-DCコンバータを使って3.3Vの電源で駆動している。スタンバイ電流の小さなDC-DCコンバータを選べば、deep-sleep時の消費電力を**8V時,1mA**程度に抑えることができ、電池の消耗を抑えることが可能である。

**PIX-MT100**は**5V**の電源が必要になるので、5V用のDC-DCコンバータを別途用意し、必要なときだけ電力を供給できるように**N-ch MOS FET**で電源を制御するようにしている。回路的には5V単一電源だけでも構成可能なのだが、deep-sleep時の消費電力が大きくなってしまう(**8V時,9mA程度**)ため個別に電源を供給するようにしている。

**Raspberry pi zero W**で構成したシステムと比較すると、

- **安価である**：**Raspberry pi zero W**と比べると半分以下の価格で構成することが可能である。専用の基板等を作成して、量産を行えるようなら、コストは**1/3**から**1/4**にすることが可能ではないかと思われる。
- **安定性が高い**：**Raspberry pi zero W**は、小さいとは言えフルの**Linux**が動作しているため、起動するまでには数多くのソフトウエアが介在する。**NodeMCU-32S**の場合、ブートローダーから直接起動されるので非常にシンプルな構成にすることができる。そのためソフトウエア的な安定性は圧倒的に**NodeMCU-32S**の方が安定的である。
- **低消費電力**：**Raspberry pi zero W**では待機時の消費電力が**10mA**程度あり、電池駆動の場合は決して無視できるものではなかったが、**NodeMCU-32S**では**1mA**と小さく、同じ電池容量であれば駆動時間が数倍から１０倍程度まで伸ばせることを意味する。
- **電池駆動に適している**：消費電力が少ないため**NodeMCU-32S**は、電池駆動に適しているが、**NodeMCU-32S**は電源電圧が**2.2V**程度まで動作可能である。上記の回路は**8V**から**3.3V**に降圧して電源を供給しているが、これを**乾電池２本**直列**(3V)**で動作させることも可能である。
  **PIX-MT100**を駆動する必要がなければ、乾電池で駆動することも十分可能である。

と言った点があげられる。

また、内蔵する**ADC**を使用して電池の電源電圧を測定し、電源電圧を測定できるようにしている。これによって電池の過放電を防止できるようになる。

## ▼ 変わり種ESP32

![IMG_0033](./img/img_0033.jpg)

**ESP32**に小さな**有機EL**のディスプレイを搭載したもの。測定値をすぐにモニターできるので、用途によっては便利だと思う。これに**INA226**をつけて、電圧・電流・消費電力計を作ってみようと思っている。カメラで撮ると少し汚く写っているが、実際にはもう少しきれいに表示されている。価格は2200円程度と少し高め。

![IMG_0034](./img/img_0034.jpg)

これも少し変わったArduino互換ボードである。USBコネクタの大きさから解るように非常に小さい。ネットワーク機能などはないが、組み込み的に使うのには好適ではないかと思われる。入手したのはESP32よりも後。当初はもう少し早く来るはずだったのだが、indigogoというスタートアップのサイトから注文したものだったので、製品ができるまで少々遅れがあったようだ(届かなくても仕方がないかなと思っていたので、まあ届いて良かった)。

![mg_004](./img/img_0047.jpg)

これは、ESP-WROOM-32用のモジュールコネクタが付いたボード。ブレッドボードで使う場合、NodeMCU-32Sのようなモジュールを使うことが多いが、消費電力の観点からはESP-WROOM-32そのもののみを実装したほうが良い場合がある。その場合、このようなボードで予めプログラミングしてから実装するような方法もありえる。またチップ部分のみ交換して使えるので、複数のチップを交換して使う用途にも向きそうだ。

## ▼ Wemos Lolin ESP32 + BME280

![img_0035](./img/img_0035.jpg)

**wemos Lolin ESP32**に**BME280**を接続し、**気温・湿度・気圧**を測定できるようにしたもの。この構成にすると、測定データをOLEDディスプレイに表示しながら、データをWi-Fi経由でmuninサーバに送るといった方法が使える。とりあえず今回は**wemos Lolin ESP32**の**I2C**経由で測定できるかどうかの検証を行った。**wemos Lolin ESP32**はOLEDディスプレイの接続に**I2C**を使用しているので、**I2C**で使用するデータピンが固定されているのでライブラリとの整合性に不安があったが、検証の結果問題なく使用できることがわかった。

## ▼ Lolion32を使った計測ボード

先に**wemos Lolin ESP32**に**BME280**をつけたものを紹介したが、それだけではつまらないので、手持ちの**I2C**デバイスを３つ接続してみた。

![img_0036](./img/img_0036.jpg)

![img_0037](./img/img_0037.jpg)

![img_0038](./img/img_0038.jpg)

まだ余裕があったので、あと２つ追加してみた。

![](./img/img_0041.jpg)

上の写真は、**BME280(気温、湿度、気圧)**,**LIS3DH(３軸加速度計)**,**INA226(電圧、電流、消費電力)**、**ADT7410(温度計)**、**DS3231(リアルタイムクロック)**の５つを接続した時の表示である。この写真では見にくいが、LIS3DHの３軸の加速度を表示してところである。

記録のためスケッチを載せておく。

**multi_meter.h**

```C++
/*
  multi_meter header
*/

#define I2C_SDA 5                                     // SDA Pin No.5 yellow
#define I2C_SCL 4                                     // SCL Pin No.4 green
// #define WIRE_CLOCK 400000                             // I2C clock speed
#define LINE_OFFSET 4                                 // header offset lines
#define DISPLAY_TIMES 20                              // how many cycle
#define DISPLAY_WAIT 250                              // wait time par cycle
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)  // BME280 presser senser sealevel adjustment value

```

**multi_meater.ino**

```C++
/*
  INA226,BME280 multi-meter
*/

#include <Wire.h>
#include "SSD1306.h"
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"
#include "multi_meter.h"

SSD1306 display(0x3c, I2C_SDA, I2C_SCL);

int bme280_disable;
int lis3dh_disable;
int ina226_disable;
int adt7410_disable;
int ntp_disable;
int adc_disable;

int ssd1306_init() {
  display.init();
  display.flipScreenVertically();
  display.clear();
  display.setFont(ArialMT_Plain_16);
  // display.setFont(Monospaced_plain_16);
  display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
  display.drawString(64, 32, "Multi-Meter");
  display.display();
  return 0;
}

void setup() {
  Serial.begin(115200);
  ssd1306_init();
  bme280_disable  = bme280_init();
  lis3dh_disable  = lis3dh_init();
  ina226_disable  = ina226_init();
  adt7410_disable = adt7410_init();
  ntp_disable     = ntp_init();
  adc_disable     = adc_init();
  delay(1000);
}

void loop() {
  Wire.reset();
  if (bme280_disable  == 0) {
    bme280_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (adt7410_disable == 0) {
    adt7410_display(DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (ina226_disable  == 0) {
    ina226_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (true) {
    adc_display    (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (true) {
    ntp_display    (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (lis3dh_disable  == 0) {
    lis3dh_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  Serial.println();
}
```

**adc.ino**

```c++
/*
  ADC
*/

#include "multi_meter.h"

#define ADC_ADJUSTMENT (1.06)

int adc_init() {
  return 0;
}

void adc_display(int times, int wait) {
  char strbuf[64];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int volt_pos = (16 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // 電圧、電流、電力読込み
    int adcv      = (int)(3.3 / 4096 * analogRead(A3) * 3 * 1000 * ADC_ADJUSTMENT);         // ADC(ADC3,SVN pin)

    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "ADC");

    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, volt_pos, "Volt");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, volt_pos, "mV");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5ld", adcv);
    display.drawString(data_pos, volt_pos, strbuf);

    if (not done) {
      Serial.printf("V:%5ldmV ", adcv);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**bme280.ino**

```C++
/*
  BME280
*/

#include <Wire.h>
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"
#include "multi_meter.h"

#define BME280_ADD 0x76

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

int bme280_init() {
  bme.begin(BME280_ADD);
  return 0;
}

void bme280_display(int times, int wait) {
  char strbuf[100];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int temp_pos = (16 - LINE_OFFSET);
  int humi_pos = (32 - LINE_OFFSET);
  int pres_pos = (48 - LINE_OFFSET);
  bool done = false;

  // BME280
  for (int i = 0; i < times; i ++) {
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "BME280");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_10);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, temp_pos, "Temp");
    display.drawString(head_pos, humi_pos, "Humi");
    display.drawString(head_pos, pres_pos, "P:slvl");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, temp_pos, "ºC");
    display.drawString(unit_pos, humi_pos, "%");
    display.drawString(unit_pos, pres_pos, "hPa");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", bme.readTemperature());
    display.drawString(data_pos, temp_pos, strbuf);
    sprintf(strbuf, "%5.1f", bme.readHumidity());
    display.drawString(data_pos, humi_pos, strbuf);
    sprintf(strbuf, "%5.1f", bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
    display.drawString(data_pos, pres_pos, strbuf);

    if (not done) {
      Serial.printf("T:%5.1fºC,H:%5.1f%%,P:%5.1fhPa ", bme.readTemperature(), bme.readHumidity(), bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**ina226.ino**

```C++
/*
  INA226
*/

#include <Wire.h>
#include "multi_meter.h"

#define INA226_ADD (0x40)

// I2c通信送信
void ina226_writeRegister(byte reg, word value) {
  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  Wire.write((value >> 8) & 0xFF);
  Wire.write(value & 0xFF);
  Wire.endTransmission();
}

// I2c通信受信
word ina226_readRegister(byte reg) {
  word res = 0x0000;

  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  if (Wire.endTransmission() == 0) {
    if (Wire.requestFrom(INA226_ADD, 2) >= 2) {
      res  = Wire.read() * 256;
      res += Wire.read();
    }
  }
  return res;
}

int ina226_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  ina226_writeRegister(0x00, 0x4127);
  ina226_writeRegister(0x05, 0x0A00);
  return 0;
}

void ina226_display(int times, int wait) {
  char strbuf[64];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int volt_pos = (16 - LINE_OFFSET);
  int crnt_pos = (32 - LINE_OFFSET);
  int pwer_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // 電圧、電流、電力読込み
    long voltage  = (long)((short)ina226_readRegister(0x02)) * 1250L;    // LSB=1.25mV
    short current = (short)ina226_readRegister(0x04);
    long power    = (long)ina226_readRegister(0x03) * 25000L;            // LSB=25mW

    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "INA226");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, volt_pos, "Volt");
    display.drawString(head_pos, crnt_pos, "Current");
    display.drawString(head_pos, pwer_pos, "Power");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, volt_pos, "mV");
    display.drawString(unit_pos, crnt_pos, "mA");
    display.drawString(unit_pos, pwer_pos, "mW");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5ld", (voltage + (1000 / 2)) / 1000);
    display.drawString(data_pos, volt_pos, strbuf);
    sprintf(strbuf, "%5ld", current);
    display.drawString(data_pos, crnt_pos, strbuf);
    sprintf(strbuf, "%5ld", (power + (1000 / 2)) / 1000);
    display.drawString(data_pos, pwer_pos, strbuf);

    if (not done) {
      Serial.printf("V:%5ldmV,I:%5ldmA,P:%5ldmW ", (voltage + (1000 / 2)) / 1000, current, (power + (1000 / 2)) / 1000);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**lis3dh.ino**

```C++
/*
  LIS3DH
*/

#include <Wire.h>
#include "multi_meter.h"

#define LIS3DH_ADD 0x19

#define DEFAULT_XA (0.0)
#define DEFAULT_YA (0.0)

unsigned int lis3dh_readRegister(byte reg) {
  Wire.beginTransmission(LIS3DH_ADD);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(LIS3DH_ADD, 1);
  return Wire.read();
}

void lis3dh_writeRegister(byte reg, byte data) {
  Wire.beginTransmission(LIS3DH_ADD);
  Wire.write(reg);
  Wire.write(data);
  Wire.endTransmission();
}

int lis3dh_init() {
  lis3dh_writeRegister(0x20, 0x27);
  int res = lis3dh_readRegister(0x0f);
  return 0;
}

int s18(unsigned int v) {
  return -(v & 0b100000000000) | (v & 0b011111111111);
}

void lis3dh_display(int times, int wait) {
  char strbuf[100];
  unsigned int x, y, z, h, l;
  float xa, ya, za;
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int galx_pos = (16 - LINE_OFFSET);
  int galy_pos = (32 - LINE_OFFSET);
  int galz_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // LIS3DH
    lis3dh_writeRegister(0x20, 0x27);
    // X
    l = lis3dh_readRegister(0x28);
    h = lis3dh_readRegister(0x29);
    x = (h << 8 | l) >> 4;
    xa = s18(x) / 1024.0 * 980.0 - DEFAULT_XA;
    // Y
    l = lis3dh_readRegister(0x2a);
    h = lis3dh_readRegister(0x2b);
    y = (h << 8 | l) >> 4;
    ya = s18(y) / 1024.0 * 980.0 - DEFAULT_YA;
    // Z
    l = lis3dh_readRegister(0x2c);
    h = lis3dh_readRegister(0x2d);
    z = (h << 8 | l) >> 4;
    za = s18(z) / 1024.0 * 980.0;
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "LIS3DH");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, galx_pos, "Xaxis");
    display.drawString(head_pos, galy_pos, "Yaxis");
    display.drawString(head_pos, galz_pos, "Zaxis");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", xa);
    display.drawString(data_pos, galx_pos, strbuf);
    sprintf(strbuf, "%5.1f", ya);
    display.drawString(data_pos, galy_pos, strbuf);
    sprintf(strbuf, "%5.1f", za);
    display.drawString(data_pos, galz_pos, strbuf);

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, galx_pos, "Gal");
    display.drawString(unit_pos, galy_pos, "Gal");
    display.drawString(unit_pos, galz_pos, "Gal");

    if (not done) {
      Serial.printf("X:%5.1fGal,Y:%5.1fGal,Z:%5.1fGal ", xa, ya, za);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**adt7410.ino**

```C++
/*
 ADT7410
*/

#include <Wire.h>
#include "multi_meter.h"

#define ADT7410_ADD 0x48

int adt7410I2CAddress = 0x48;

int adt7410_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  return 0;
}

unsigned int adt7410_readRegister(void) {
  unsigned int val;

  Wire.requestFrom(ADT7410_ADD, 2);
  val  = Wire.read() << 8;
  val |= Wire.read();
  return val;
}

// メインループ
void adt7410_display(int times, int wait) {
  char strbuf[100];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int temp_pos = (16 - LINE_OFFSET);
  uint16_t uiVal; //2バイト(16ビット)の領域
  float fVal;
  int iVal;
  bool done = false;

  for (int i = 0; i < times; i ++) {
    uiVal = adt7410_readRegister();
    uiVal >>= 3;                          // シフトで13bit化
    if (uiVal & 0x1000) {                 // 13ビットで符号判定
      iVal = uiVal - 0x2000;              // マイナスの時 (10進数で8192)
    } else {
      iVal = uiVal;                       //プラスの時
    }
    fVal = (float)iVal / 16.0;            // 温度換算(摂氏)
    //
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "ADT7410");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, temp_pos, "Temp");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, temp_pos, "ºC");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", fVal);
    display.drawString(data_pos, temp_pos, strbuf);

    if (not done) {
      Serial.printf("T:%5.1fºC ", fVal);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**ntpclient.ino**

```C++
/*
  NTP client
*/

#include <WiFi.h>
#include <DS3231.h>

#include "multi_meter.h"

#define JST (3600L * 9)

const char *ssid = "********";
const char *password = "********";
const char *ntp_servers[] { "ntp.nict.jp", "ntp.jst.mfeed.ad.jp", "time.google.com" };

struct tm timeInfo;
DS3231 rtc;
bool rtc_century = false;
bool rtc_h12, rtc_pm;

char *dayofweek[] = { "Sunday", "Monday", "Tuesday", "Wednsday", "Thursday", "Friday", "Saturday" };
char *dayofweek_short[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

int ntp_init() {
  int count;

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  if (WiFi.begin(ssid, password) != WL_DISCONNECTED) {
    ESP.restart();
  }
  count = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    count ++;
    if (count > 5) {
      return -1;
    }
  }
  configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
  // sprintf(datebuf,"%02d/%02d/%02d(%s)",timeInfo.tm_year + 1900 - 2000, timeInfo.tm_mon + 1, timeInfo.tm_mday,dayofweek_short[timeInfo.tm_wday]);
  // sprintf(timebuf,"%02d:%02d:%02d",timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
  getLocalTime(&timeInfo);
  rtc.setClockMode(false);  // set to 24h
  rtc.setYear(timeInfo.tm_year + 1900);
  rtc.setMonth(timeInfo.tm_mon + 1);
  rtc.setDate(timeInfo.tm_mday);
  rtc.setDoW(timeInfo.tm_wday);
  rtc.setHour(timeInfo.tm_hour);
  rtc.setMinute(timeInfo.tm_min);
  rtc.setSecond(timeInfo.tm_sec);
  return 0;
}

void ntp_ticker() {
  char strbuf[100];

  // Serial.print(".");
  if (!ntp_disable) {
    getLocalTime(&timeInfo);
    // sprintf(strbuf, "%02d/%02d(%s) %02d:%02d:%02d",
    //         timeInfo.tm_mon + 1, timeInfo.tm_mday, dayofweek_short[timeInfo.tm_wday],
    //         timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    sprintf(strbuf, "%02d/%02d %02d:%02d:%02d",
            timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);

    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 0, strbuf);
    if (timeInfo.tm_hour == 0 && timeInfo.tm_min == 0 && timeInfo.tm_sec == 0) {
      configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
      getLocalTime(&timeInfo);
      rtc.setClockMode(false);  // set to 24h
      rtc.setYear(timeInfo.tm_year + 1900);
      rtc.setMonth(timeInfo.tm_mon + 1);
      rtc.setDate(timeInfo.tm_mday);
      rtc.setDoW(timeInfo.tm_wday);
      rtc.setHour(timeInfo.tm_hour);
      rtc.setMinute(timeInfo.tm_min);
      rtc.setSecond(timeInfo.tm_sec);
      delay(1000);
    }
  } else {
    int year  = rtc.getYear();
    int mon   = rtc.getMonth(rtc_century);
    int day   = rtc.getDate();
    int hour  = rtc.getHour(rtc_h12, rtc_pm);
    int min   = rtc.getMinute();
    int sec   = rtc.getSecond();
    char *dow = dayofweek_short[rtc.getDoW()];
    // sprintf(strbuf, "%02d/%02d(%s) %02d:%02d:%02d", mon, day, dow, hour, min, sec);
    sprintf(strbuf, "%02d/%02d %02d:%02d:%02d", mon, day, hour, min, sec);

    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 0, strbuf);
  }
  display.drawLine(0, 11, 128, 11);
}

void ntp_display(int times, int wait) {
  char datebuf[20], timebuf[20];
  int head_pos =  0;
  int data_pos = 128;
  int date_pos = (16 - LINE_OFFSET);
  int time_pos = (32 - LINE_OFFSET);
  int week_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    if (ntp_disable == 0) {
      getLocalTime(&timeInfo);
      sprintf(datebuf, "%04d/%02d/%02d", timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday);
      sprintf(timebuf, "%02d:%02d:%02d", timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    } else {
      int year  = rtc.getYear() + 1952;
      int mon   = rtc.getMonth(rtc_century);
      int day   = rtc.getDate();
      int hour  = rtc.getHour(rtc_h12, rtc_pm);
      int min   = rtc.getMinute();
      int sec   = rtc.getSecond();
      char *dow = dayofweek_short[rtc.getDoW()];
      sprintf(datebuf, "%04d/%02d/%02d", year, mon, day);
      sprintf(timebuf, "%02d:%02d:%02d", hour, min, sec);
    }
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    if (ntp_disable == 0) {
      display.drawString(0, 0, "NTP");
    } else {
      display.drawString(0, 0, "DS3231");
    }
    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, date_pos, "Date");
    display.drawString(head_pos, time_pos, "Time");
    display.drawString(head_pos, week_pos, "Week");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(data_pos, date_pos, datebuf);
    display.drawString(data_pos, time_pos, timebuf);
    if (ntp_disable == 0) {
      display.drawString(data_pos, week_pos, dayofweek[timeInfo.tm_wday]);
    } else {
      display.drawString(data_pos, week_pos, dayofweek[rtc.getDoW()]);
    }

    if (not done) {
      Serial.printf("DATE:%s,TIME:%s ", datebuf, timebuf);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

センサー毎にソースファイルを分割している。arduino IDEはヘッダファイルを作成しなくとも自動的にプロトタイプ宣言を作ってコンパイルしてくれるようなので、ファイルを分割してもそれほど手間がかからない。

**Wemos Lolin ESP32**はオンボードディスプレイのコントロールにI2Cを使用しているのだが、標準的なライブラリとは使用しているピンが異なる。そのため、標準的なピン接続を想定しているライブラリは動かない事が多い。そのため、ピン番号を指定できる**BME280**以外はarduino用にダウンロードしたライブラリではなく、**Wireライブラリ**を直接使用して計測を行っている。

## ▼ OLEDの表示が止まる

こんな形でスケッチを作成し、**Wemos Lolin ESP32**にプログラムを転送しテストしていたところ、いつの間にか**OLEDの表示が止まる**という現象が発生していることに気がついた。いろいろ調べてみると、

- 電源のノイズで誤動作している
- I2Cバスのプルアップ抵抗が無い
- I2Cのクロックが高すぎる

などが考えられた。で、パスコンを入れたり、プルアップ抵抗を追加したり、`Wire.setClock()`でI2Cバスのクロックを下げてみたりとやってみたのだが、どうも事態は改善しない。

なにか情報はないかと色々ググっていたら、[ESP32 の I2C は壮絶にバグってる](http://d.hatena.ne.jp/wakwak_koba/touch/20171228)という記事を見つけた。どうもこれくさいということで、

**multi-meter.ino**

```C++
...
void loop() {
  Wire.reset();
  if (bme280_disable  == 0) {
    bme280_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  ...
}
```

というふうに適当なタイミングでI2Cインターフェースをリセットするようにしてみた。これで事態が改善するようなら今後OLED付き以外でも大量のデータ転送を行う場合には適当なところで`Wire.reset()`を行うようにするのが良いようだ。

## ▼ NTPとDS3231RTC

とりあえず、手持ちのI2Cデバイスを全てつけてみようということで、時計の表示も追加した。基本的にはNTPを使って時刻を取得して表示するのだが、Wi-Fiの届かないところでは時刻が取得できない。そこで手持ちのDS3231RTCモジュールを接続し、

- NTPで時刻が取得できたらRTCもその時刻に同期
- NTPで時刻が取得できない場合はRTCから時刻を取得

という動作にしてみた。こうすれば、Wi-Fiで接続できる自宅ではNTP時刻を使い、屋外などNTPサーバに接続できないときはRTCを使って時刻を表示することができるようになる。RTCはNTPに接続した時にNTPの時刻と同期しているので、数日毎にNTPで時刻を取得してやれば、かなり正確な時刻を取得できる。

## ▼ adc.ino

これは、ESP32**内臓ADC**(ADコンバータ)を使用して、電池の電源電圧を測定するものである。回路的にはかなり怪しいものであるが、この構成では正常に動作しているようだ(電位差のある別電源系に接続しているので、本来の電源系やUSBの電源供給に悪影響を及ぼす可能性がある)。

**内臓ADC**はフルスケール(12bit=4086)で**3.3V**に対応しているので、バッテリー側の電源を抵抗で**1/3に分圧**して測定し、その値を**３倍**することで実際の電圧を測定している。**内蔵ADC**はあまり精度が良くないようなので、正確な値を測定することは難しいが、電源電圧を監視し、スレッショルド値以下なら起動しないようにするなどの**電源保護**(リチウムイオン電池は3.0V以下まで放電すると**充電できなくなる**)には使えそうである。
