# ■ Arduino IDE on Raspberry Pi

----

## ▼ ESP32開発環境のインストール

```shell
sudo usermod -a -G dialout $USER && \
sudo apt-get install git && \
wget https://bootstrap.pypa.io/get-pip.py && \
sudo python get-pip.py && \
sudo pip install pyserial && \
mkdir -p ~/Arduino/hardware/espressif && \
cd ~/Arduino/hardware/espressif && \
git clone https://github.com/espressif/arduino-esp32.git esp32 && \
cd esp32/tools/ && \
python get.py
```

本来ならこれだけでOKなはずだが、現状ではクロスコンパイラのインストールができない状態で終わってしまう。

## ▼ aptパッケージのインストール

```shell
sudo apt-get install git wget make libncurses-dev flex bison gperf python python-serial
sudo apt-get install gawk gperf grep gettext python python-dev automake bison flex texinfo help2man libtool libtool-bin
```

## ▼ crosstool-NGのダウンロード＆ビルド

```shell
mkdir ~/esp32
cd ~/esp32
git clone -b xtensa-1.22.x https://github.com/espressif/crosstool-NG.git
cd crosstool-NG
./bootstrap && ./configure --enable-local && make install
```

なので、クロスコンパイラを別途インストールし、

## ▼ Toolchainのビルド

```shell
./ct-ng xtensa-esp32-elf
./ct-ng build
chmod -R u+w builds/xtensa-esp32-elf
```

## ▼ ESP-IDFの取得

```shell
cd ~/esp32
git clone --recursive https://github.com/espressif/esp-idf.git
```

## ▼ 環境変数の設定(.bashrc)

```shell
export IDF_PATH=$HOME/esp32/esp-idf
export PATH=$PATH:$HOME/esp32/crosstool-NG/builds/xtensa-esp32-elf/bin
```

## ▼ Toolchainのリンク

```shell
ln -s /home/pi/esp32/crosstool-NG/builds/xtensa-esp32-elf /home/pi/Arduino/hardware/espressif/esp32/tools/xtensa-esp32-elf
```

ARduino IDEのtoolsから呼び出せるようにリンクを貼ると正常にESP32向けのコンパイルができるようになる。

