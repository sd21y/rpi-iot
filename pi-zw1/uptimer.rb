#!/usr/bin/ruby

interval = 3600
limit = 0
time = 0
File.open("./capacity.txt", "r") {|fd| limit = (fd.gets().to_f * 21.6).to_i }
begin
  File.open("./uptime.txt","r") {|fd| time  = fd.gets().to_i }
rescue
  time = 0
  `./pwonmail.sh`
end
time += interval
File.open("./uptime.txt","w")  {|fd| fd.puts(time.to_s) }
`./measure.sh`
if(time >= limit)
  `./pwoffmail.sh`
  File.unlink("./uptime.txt")
  `/sbin/poweroff`
end
